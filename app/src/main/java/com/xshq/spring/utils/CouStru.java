package com.xshq.spring.utils;

public class CouStru {
    static{
        System.loadLibrary("countjni");
    }

    public native static float CountGetAMRForMS1(float Bmr,float Weight,float Height,int Age, int Sex, int activity) ;
    public native static float CountGetVisceralFatForMS1(float Fat,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetSubFatForMS1(float Fat,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetBodyAgeForMS1(float Fat,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetProteinForMS1(float Muscle,float Moisture,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetBodyScoreForMS1(float Fat,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetSkeletalMuscleForMS1(float Moisture,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetBodyTypeForMS1(float Fat,float Muscle,float Weight,float Height,int Age, int Sex) ;

    public native static float CountGetFat(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMoisture(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMuscle(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMuscleMass(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetSkeletalMuscle(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBoneMass(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBMR(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetAMR(float Impedance,float Weight,float Height,int Age, int Sex, int activity) ;
    public static native float CountGetVisceralFat(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetSubFat(float Impedance,float Weight,float Height,int Age, int Sex);
    public static native float CountGetBodyAge(float Impedance,float Weight,float Height,int Age, int Sex,int activity);
    public static native float CountGetProtein(float Impedance,float Weight,float Height,int Age, int Sex);
    public static native float CountGetBodyScore(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBodyType(float Impedance,float Weight,float Height,int Age, int Sex) ;

    public native static float CountGetFat2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMoisture2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMuscle2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public native static float CountGetMuscleMass2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetSkeletalMuscle2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBoneMass2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBMR2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetAMR2(float Impedance,float Weight,float Height,int Age, int Sex, int activity) ;
    public static native float CountGetSubFat2(float Impedance,float Weight,float Height,int Age, int Sex);
    public static native float CountGetBodyAge2(float Impedance,float Weight,float Height,int Age, int Sex ,int activity);
    public static native float CountGetProtein2(float Impedance,float Weight,float Height,int Age, int Sex);
    public static native float CountGetBodyScore2(float Impedance,float Weight,float Height,int Age, int Sex) ;
    public static native float CountGetBodyType2(float Impedance,float Weight,float Height,int Age, int Sex) ;


    //------------------------------------------------------------------------
    //新算法 AC 0 是交流算法，1 是直流算法
    //Sex  ：1 为男 ，0 为女
    //脂肪
    public native static float CountGetFat3(float Impedance,float Weight,float Height,int Age, int Sex, int Ac) ;
    //水分
    public native static float CountGetMoisture3(float Impedance,float Weight,float Height,int Age, int Sex, int Ac) ;
    //肌肉
    public native static float CountGetMuscle3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //肌肉量
    public native static float CountGetMuscleMass3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //骨骼肌
    public static native float CountGetSkeletalMuscle3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //骨骼率
    public static native float CountGetBoneMass3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //基础代谢率
    public static native float CountGetBMR3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //活动代谢
    public static native float CountGetAMR3(float Impedance,float Weight,float Height,int Age, int Sex, int activity,int Ac) ;
    //内脏脂肪指数
    public static native float CountGetVisceralFat3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac);
    //皮下脂肪
    public static native float CountGetSubFat3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac);
    //身体年龄算法
    public static native float CountGetBodyAge3(float Impedance,float Weight,float Height,int Age, int Sex, int activity, int ac);
    //蛋白率
    public static native float CountGetProtein3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac);
    //健康评分
    public static native float CountGetBodyScore3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;
    //体型判断
    public static native float CountGetBodyType3(float Impedance,float Weight,float Height,int Age, int Sex,int Ac) ;

    //脂肪量kg
    public static native float CountGetFatWeight3(float Impedance,float Weight,float Height,int Age, int Sex,int ac) ;
    //皮下脂肪量kg
    public static native float CountGetSubFatWeight3(float Impedance,float Weight,float Height,int Age, int Sex,int ac);
    //水分量kg
    public native static float CountGetMoistureWeight3(float Impedance,float Weight,float Height,int Age, int Sex, int ac) ;
    //骨量kg
    public static native float CountGetBoneMassWeight3(float impedance,float weight,float height,int age,int sex,int ac);
    //蛋白质kg
    public static native float CountGetProteinWeight3(float Impedance,float Weight,float Height,int Age, int Sex,int ac);
    //骨骼肌量kg
    public static native float CountGetSkeletalMuscleWeight3(float Impedance,float Weight,float Height,int Age, int Sex,int ac) ;
    //版本
    public static native float CountGetVersion() ;


    //Bmi
    public static native float CountGetbmi(float weight,float height) ;
    /* 八电极的体型获取
     fat: 脂肪率
     weight：体重
     muscle：肌肉率
     height：身高
     age：年龄
     sex：性别 1 为男 ，0 为女
   */
    public static native float CountBodyTypeByEight(float fat,float weight,float muscle,float height,int age,int sex);
    /* 八电极的年龄获取
       fat: 脂肪率
       visceralFat：内脏脂肪指数
       muscle：肌肉率
       height：身高
       age：年龄
       sex：性别  1 为男 ，0 为女
       activity：运动等级 1表基本无活动，2表办公(轻度)，3表常站立(中度),4表体力劳动（中强）,5表运动员（强）
     */
    public static native float CountBodyAgeByEight(float fat,float visceralFat,float muscle,float height,int age,int sex ,int activity );


    /****************************************************************************************
     * 单机版IF912B bodyMointer算法
     * 透传直流秤体 算法
     *****************************************************************************************/
    //脂肪 直流
    public static native float  CountGetFatSingle
    (float impedance,float weight,float height,int age,int sex);

    //水分 直流
    public static native float  CountGetMoistureSingle
    (float impedance,float weight,float height,int age,int sex);

    //肌肉 直流
    public static native float  CountGetMuscleSingle
    (float impedance,float weight,float height,int age,int sex);

    //骨骼率
    public static native float  CountGetBoneMassSingle
    (float impedance,float weight,float height,int age,int sex);

    //基础代谢率
    public static native float  CountGetBMRSingle
    (float impedance,float weight,float height,int age,int sex);

    //活动代谢
    public static native float  CountGetAMRSingle
    (float impedance,float weight,float height,int age,int sex,int activity) ;


    //蛋白率
    //public static native float  CountGetProteinSingle
    //        (float bodyMuscle,float bodyHydro);

    //皮下脂肪
    public static native float  CountGetSubFatSingle
    (float impedance,float weight,float height,int age,int sex ,float bodyFat);

    //身体年龄算法
    public static native float  CountGetBodyAgeSingle
    (float impedance,float weight,float height,int age,int sex ,int activity, float fat ,float bodyMuscle);

    //健康评分
    public static native float  CountGetBodyScoreSingle
    (float impedance,float weight,float height,int age,int sex ,float bodyFat);

    //体型判断
    public static native int  CountGetBodyTypeSingle
    (float impedance,float weight,float height,int age,int sex, float fat, float muscle);

    //脂肪量，单位kg
    public static native float  CountGetFatSingleW
    (float bodyFat,float weight);

    //皮下脂肪量，单位kg
    public static native float  CountGetSubFatSingleW
    (float subFat,float weight);

    //水分量，单位kg
    public static native float  CountGetMoistureSingleW
    (float Hydro,float weight);

    //肌肉量
    public static native float  CountGetMuscleMassSingleW
    (float bodyMuscle,float weight);

    //骨量，单位kg 骨骼重量
    public static native float  CountGetBoneMassSingleW
    (float bodyBone,float weight);

    //蛋白质量，单位kg
    //public static native float  CountGetProteinSingleW
    //        (float protein,float weight);

}
