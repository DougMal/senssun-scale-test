package com.emagrecebrasil.scaletestbank.ui.signup

data class FilledData(
        var textFirebaseId: String,
        val textNome: String,
        val textSobrenome: String,
        val textEmail: String,
        val textSenha: String,
        val textAltura: String,
        val textNascimento: String,
        val textPesoDesejado: String,
        val intActivity: Int,
        val textGenero: String
)