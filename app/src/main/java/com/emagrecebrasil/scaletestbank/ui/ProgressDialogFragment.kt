package com.emagrecebrasil.scaletestbank.ui

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import com.emagrecebrasil.scaletestbank.R
import kotlinx.android.synthetic.main.progress_spin.view.*

class ProgressDialogFragment: DialogFragment() {

    private lateinit var pBar: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.progress_spin, container, false)

        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        pBar = root.progress_bar
        pBar.indeterminateDrawable.setColorFilter(Color.parseColor("#D47FA6"), PorterDuff.Mode.MULTIPLY)
        return root
    }
}