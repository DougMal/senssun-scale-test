package com.emagrecebrasil.scaletestbank.ui.signup

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.databinding.FragmentCadastroBinding
import com.emagrecebrasil.scaletestbank.ui.ProgressDialogFragment
import com.emagrecebrasil.scaletestbank.util.USER_ID
import kotlinx.android.synthetic.main.fragment_cadastro.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates


class CadastroFragment : Fragment() {

    private var cal = Calendar.getInstance()
    private var userSavedSuccess: Boolean = false
    private var firebaseUserId: String = ""
    private var gender: String = ""
    private var isNetworkAvailable by Delegates.notNull<Boolean>()
    private lateinit var filledData: FilledData
    private lateinit var navController: NavController
    private lateinit var cadastroProgressBar: ProgressDialogFragment
    private lateinit var toolbar: Toolbar
    private lateinit var checkAgree: CheckBox
    private lateinit var termsUse: TextView
    private lateinit var privacyPolicy: TextView
    private lateinit var policyText: String
    private lateinit var termsText: String
    private var activityValue: Int? = null

    private var _binding : FragmentCadastroBinding? = null
    private val binding get() = _binding!!

    private val cViewModel by viewModel<CadastroViewModel>()

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCadastroBinding.inflate(inflater, container, false)
        val view = binding.root
        cadastroProgressBar = ProgressDialogFragment()

        toolbar = binding.cadastroToolbar.toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val toolText = binding.cadastroToolbar.toolbarTitle
        toolText.setText(R.string.act_cadastro_title)
        val toolEmpty = (activity as AppCompatActivity).supportActionBar
        toolEmpty!!.setDisplayShowTitleEnabled(false)

        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        observeEvents()

        if (cViewModel.checkExistUserTable()) {
            val currentUser = cViewModel.getUser()
            binding.editNomeUsuario.text = currentUser.name.toEditable()
            binding.editSobrenomeUsuario.text = currentUser.surname.toEditable()
            binding.editEmailUsuario.text = currentUser.email.toEditable()
            binding.editSenhaUsuario.text = currentUser.password.toEditable()
            binding.editTextHeight.text = currentUser.height.toString()
            binding.spinBirth.text = currentUser.birthday
            binding.editWeightTarget.text = currentUser.desiredWeight.toString()
            binding.editActivityLevel.text = ACTIVITY_LEVEL[currentUser.activityLevel - 1]
            if (currentUser.gender == "Female") {
                binding.radioBtnFemale.isChecked = true
                gender = "Female"
            } else {
                binding.radioBtnMale.isChecked = true
                gender = "Male"
            }
            binding.buttonCadastrar.text = resources.getString(R.string.act_update_btn)
            setListeners()

        }else {
            binding.buttonCadastrar.text = resources.getString(R.string.act_cadastro_btn)
            setListeners()
        }
    }

    private fun observeEvents() {
        cViewModel.cadastroState.observe(viewLifecycleOwner) { cadastroState ->
            when (cadastroState) {
                is CadastroViewModel.CadastroState.UserSaved,
                -> {
                    Toast.makeText(
                        requireContext(),
                        R.string.act_cadastro_success,
                        Toast.LENGTH_LONG
                    ).show()
                    goToLandingPage()
                }
            }
        }

        cViewModel.fbExceptionNotify.observe(viewLifecycleOwner) { fbReturn ->
            Toast.makeText(requireContext(), fbReturn, Toast.LENGTH_LONG).show()
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setListeners() {
        // Verifica se o botão de cadastrar foi pressionado
        buttonCadastrar.setOnClickListener {
            fillData()

            if (validateNewUser()) {
                cadastroProgressBar.show(requireActivity().supportFragmentManager, "Progress")
                Log.d(TAG, "Iniciando chamada de gravação")
                CoroutineScope(Dispatchers.IO).launch {
                    cViewModel.saveUser(filledData)
                }
            }
        }

        // Check the height
        editText_Height.setOnClickListener {
            getNumberPicker(MIN_HEIGHT, MAX_HEIGHT, HEIGHT_PICKER)
            Log.d(TAG, "Texto ALTURA pós dialog: ${editText_Height.text}")
        }

        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
                Log.d(TAG, "Nascimento selecionado: ${spin_Birth.text}")
            }

        // Check if birthday field was clicked
        spin_Birth!!.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        // Check the target weight
        editWeight_target!!.setOnClickListener {
            getNumberPicker(MIN_WEIGHT, MAX_WEIGHT, WEIGHT_PICKER)
            Log.d(TAG, "Texto PESO pós dialog: ${editWeight_target.text}")
        }

        // Check the activity level
        editActivity_level!!.setOnClickListener {
            getActivityPicker()
            Log.d(TAG, "Texto ACTIVITY pós dialog: ${editActivity_level.text}")
        }

        // Check gender
        radio_group_cadastro.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioBtnFemale -> gender = "Female"

                R.id.radioBtnMale -> gender = "Male"
            }
            Log.d(TAG, "Gênero selecionado: $gender")
        }

    }

    private fun updateDateInView() {
        val myFormat = "dd/MM/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.GERMAN)
        spin_Birth.text = sdf.format(cal.time)
    }

    private fun getNumberPicker(minValue: Int, maxValue: Int, picker: Int) {
        val mContext = requireContext()
        val linearLayout = RelativeLayout(mContext)
        val aNumberPicker = NumberPicker(mContext)
        aNumberPicker.maxValue = maxValue
        aNumberPicker.minValue = minValue
        if (picker == 0) {
            aNumberPicker.value = INIT_HEIGHT
        } else {
            aNumberPicker.value = INIT_WEIGHT
        }
        val params = RelativeLayout.LayoutParams(50, 50)
        val numPickerParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        numPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL)

        linearLayout.layoutParams = params
        linearLayout.addView(aNumberPicker, numPickerParams)

        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(mContext)
        if (picker == 0) {
            alertDialogBuilder.setTitle("Select your height (cm)")
        } else {
            alertDialogBuilder.setTitle("Select your desired weight (kg)")
        }
        alertDialogBuilder.setView(linearLayout)
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton(
                "Confirm"
            ) { _, _ ->
                Log.d(TAG, "New Quantity Value : " + aNumberPicker.value)
                if (picker == 0) {
                    editText_Height.text = aNumberPicker.value.toString()
                } else {
                    editWeight_target.text = aNumberPicker.value.toString()
                }
            }
            .setNegativeButton(
                "Cancel"
            ) { dialog, _ -> dialog.cancel() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    private fun getActivityPicker() {
        val mContext = requireContext()
        val linearLayout = RelativeLayout(mContext)
        val aNumberPicker = NumberPicker(mContext)
        aNumberPicker.maxValue = ACTIVITY_LEVEL.size - 1
        aNumberPicker.minValue = 0
        aNumberPicker.value = 0
        aNumberPicker.displayedValues = ACTIVITY_LEVEL

        val params = RelativeLayout.LayoutParams(50, 50)
        val numPickerParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        numPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL)

        linearLayout.layoutParams = params
        linearLayout.addView(aNumberPicker, numPickerParams)

        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(mContext)
        alertDialogBuilder.setTitle("Select your activity level")

        alertDialogBuilder.setView(linearLayout)
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton(
                "Confirm"
            ) { _, _ ->
                editActivity_level.text = ACTIVITY_LEVEL[aNumberPicker.value]
                activityValue = aNumberPicker.value + 1
                Log.d(TAG, "New Activity Value : $activityValue")
            }
            .setNegativeButton(
                "Cancel"
            ) { dialog, _ -> dialog.cancel() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    private fun validateNewUser(): Boolean {
        var fieldCheck = false

        //Validar se os campos foram preenchidos
        if (filledData.textNome.isNotEmpty()) {
            if (filledData.textSobrenome.isNotEmpty()) {
                if (filledData.textEmail.isNotEmpty()) {
                    if (filledData.textSenha.isNotEmpty()) {
                        if (filledData.textAltura.isNotEmpty()) {
                            if (filledData.textNascimento.isNotEmpty()) {
                                if (filledData.textPesoDesejado.isNotEmpty()) {
                                    if (filledData.intActivity != null) {
                                        if (filledData.textGenero.isNotEmpty()) {
                                            fieldCheck = true
                                        } else {
                                            Toast.makeText(
                                                requireContext(),
                                                getText(R.string.cad_fill_gender),
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    } else {
                                        Toast.makeText(
                                            requireContext(),
                                            getText(R.string.cad_fill_activity),
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        getText(R.string.cad_fill_weight),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    getText(R.string.cad_fill_birth),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            Toast.makeText(
                                requireContext(),
                                getText(R.string.cad_fill_height),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            getText(R.string.cad_fill_password),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        getText(R.string.cad_fill_email),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    getText(R.string.cad_fill_surname),
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            Toast.makeText(
                requireContext(),
                getText(R.string.cad_fill_name),
                Toast.LENGTH_SHORT
            ).show()
        }
        return fieldCheck
    }


    private fun goToLandingPage() {
        cadastroProgressBar.dismiss()
        activity?.finish()
    }

    private fun fillData() {
        filledData = FilledData(
            textFirebaseId = USER_ID,
            textNome = editNomeUsuario.text.toString(),
            textSobrenome = editSobrenomeUsuario.text.toString(),
            textEmail = editEmailUsuario.text.toString(),
            textSenha = editSenhaUsuario.text.toString(),
            textAltura = editText_Height.text.toString(),
            textNascimento = spin_Birth.text.toString(),
            textPesoDesejado = editWeight_target.text.toString(),
            intActivity = activityValue!!,
            textGenero = gender
        )
    }

    fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)

    companion object {

        private val TAG = CadastroFragment::class.java.simpleName
        const val HEIGHT_PICKER = 0
        const val WEIGHT_PICKER = 1
        const val MIN_WEIGHT = 30
        const val MAX_WEIGHT = 180
        const val MIN_HEIGHT = 100
        const val MAX_HEIGHT = 250
        const val INIT_HEIGHT = 160
        const val INIT_WEIGHT = 60
        val ACTIVITY_LEVEL = arrayOf(
            "Sedentary",
            "Few Activities",
            "Moderate Activities",
            "Very Active",
            "Athlete"
        )
    }

}