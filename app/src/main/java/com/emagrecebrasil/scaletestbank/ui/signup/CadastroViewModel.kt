package com.emagrecebrasil.scaletestbank.ui.signup

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.emagrecebrasil.scaletestbank.room.entity.User
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.util.USER_ID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class CadastroViewModel(private val userRepository: UserRepository) : ViewModel() {


    private var userSavedSuccess: Boolean = false
    private lateinit var userInstance: User


    private val _cadastroState = MutableLiveData<CadastroState>()
    val cadastroState: LiveData<CadastroState>
        get() = _cadastroState

    private val _fbExceptionNotify = MutableLiveData<String>()
    val fbExceptionNotify: LiveData<String>
        get() = _fbExceptionNotify

    private val _fbAuthComplete = MutableLiveData<Boolean>()
    val fbAuthComplete: LiveData<Boolean>
        get() = _fbAuthComplete


    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun saveUser(filledData: FilledData) {
        saveDatabase(filledData)
        Log.d(TAG, "Fim coroutine Auth")
    }

    fun getUser() : User {
        return userRepository.getUser(USER_ID)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SimpleDateFormat")
    suspend fun saveDatabase(filledData: FilledData) {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = sdf.format(Date())
        userInstance = User(
                userId = filledData.textFirebaseId,
                name = filledData.textNome,
                surname = filledData.textSobrenome,
                email = filledData.textEmail,
                password = filledData.textSenha,
                dateInit = currentDate,
                birthday = filledData.textNascimento,
                height = filledData.textAltura.toInt(),
                desiredWeight = filledData.textPesoDesejado.toInt(),
                activityLevel = filledData.intActivity,
                gender = filledData.textGenero
        )

        viewModelScope.launch {
            try {
                var id = 1L
                if (userRepository.isTableExists()) {
                    userRepository.updateUser(userInstance)
                } else {
                    id = userRepository.insertUser(userInstance)
                }
                if (id > 0) {
                    userSavedSuccess = true
                    Log.d(TAG, "Usuário salvo no DB: Id = $id")
                }
            } catch (ex: Exception) {
                Log.d(TAG, "Erro ao gravar no Room-> Exception: $ex")
            }
            withContext(Dispatchers.Main) {
                _cadastroState.value = CadastroState.UserSaved
            }
        }
    }

    fun checkExistUserTable(): Boolean {
       return userRepository.isTableExists()
   }

    fun getUserListAvailable() : List<User> {
        return userRepository.getAllUsers()
    }

   sealed class CadastroState {
       object UserSaved : CadastroState()
   }

   companion object {
       private val TAG = CadastroViewModel::class.java.simpleName
   }

}