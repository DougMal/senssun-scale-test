package com.gpm.emagrecebrasil.activity.ui.measurement.config

import android.bluetooth.BluetoothProfile
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.scale.ScaleManager
import com.emagrecebrasil.scaletestbank.util.*
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import senssun.blelib.model.BleDevice
import senssun.blelib.model.WeightBean

class BTDetectFragment() : Fragment(), ScaleManager.OnScaleListener {

    private lateinit var navController: NavController

    private var bleDevice: BleDevice? = null
    private lateinit var mTempAddr: String
    private lateinit var mWeightDisplay: TextView
    private lateinit var btnRefresh : Button
    private lateinit var btnConfirm : Button
    private var jobWeight = CoroutineScope(Dispatchers.Default)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_bt_detect, container, false)

        arguments?.let {
            val safeArgs = BTDetectFragmentArgs.fromBundle(it)
            bleDevice = safeArgs.bleDeviceArg
        }
        mWeightDisplay = root.findViewById(R.id.txt_weight_adv)
        btnRefresh = root.findViewById(R.id.button_refresh)
        btnConfirm = root.findViewById(R.id.button_confirm)

        ScaleManager.init(requireContext())
        ScaleManager.getInstance().addOnScaleListener(this)
        ScaleManager.getInstance().connect(bleDevice)

        mTempAddr = bleDevice!!.bluetoothDevice.address
        Log.d(TAG, "Device: $mTempAddr")

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        btnConfirm.setOnClickListener {
            saveAddress(requireContext())
            activity?.finish()
            Log.d(TAG, "Endereço salvo: $mTempAddr")
        }

        btnRefresh.setOnClickListener {
            //activity?.onBackPressed()
            activity?.finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //ScaleManager.getInstance().removeOnScaleListener(this)
    }

    private fun saveAddress(context: Context) {
        val saveLocal = LocalConfig(context)
        val gson = Gson()
        val json = gson.toJson(bleDevice)
        saveLocal.saveConfig(SCALE_ADDRESS, bleDevice!!.bluetoothDevice.address)
        saveLocal.saveConfig(SCALE_OBJECT, json)
        saveLocal.saveConfig(SCALE_NAME, SCALE_EB)
    }


    override fun onScaleState(state: Int) {
        if (state == BluetoothProfile.STATE_CONNECTED) {
            mWeightDisplay.setTextColor(Color.BLUE)
        } else mWeightDisplay.setTextColor(Color.RED)
    }

    override fun onWeighting(weightBean: WeightBean) {
        mWeightDisplay.text = weightBean.divisionWeightKG
        val finished = weightBean.isStable
        val impedance = weightBean.zuKang
        Log.d(TAG, "Peso detectado: ${mWeightDisplay.text}")
        if (finished && impedance != 0) {
            val ssFatBean = weightBean.ssFatBean
            Log.d(TAG, "Impedância detectada: ${weightBean.zuKang}")
            Log.d(TAG, "Água: ${ssFatBean.moisture}")
            Log.d(TAG, "BMR: ${ssFatBean.bmr}")
            Log.d(TAG, "Bone: ${ssFatBean.bone}")
            Log.d(TAG, "Muscles: ${ssFatBean.muscles}")
            Log.d(TAG, "Date: ${ssFatBean.dateTime}")
            Log.d(TAG, "Heart: ${ssFatBean.heartRate}")
        }
    }

    override fun onCommandReply(p0: ScaleManager.CommandType?, p1: String?) {

    }

    companion object {
        private val TAG = BTDetectFragment::class.java.simpleName
    }



}