package com.emagrecebrasil.scaletestbank.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.futured.donut.DonutSection
import com.emagrecebrasil.scaletestbank.room.GraphRepo
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import com.emagrecebrasil.scaletestbank.scale.ScaleView
import com.emagrecebrasil.scaletestbank.util.USER_ID
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class MeasurementViewModel(
    private val weightRepository: WeightRepository,
    private val userRepository: UserRepository
) : ViewModel(), KoinComponent{

    private val scaleView: ScaleView by inject()
    private val graphDataRepo: GraphRepo by inject()

    // Additional information
    fun fetchAdditionalInfo(): MutableLiveData<AdditionalData> {
        val additionalInfo = MutableLiveData<AdditionalData>()
        viewModelScope.launch {
            graphDataRepo.getAdditionals().observeForever { addit ->
                additionalInfo.value = addit
            }
        }
        return additionalInfo
    }

    fun checkExistUserTable(): Boolean {
        return userRepository.isTableExists()
    }

    // List of data related to Donut
    private val donutSection: MutableLiveData<List<DonutSection>> = MutableLiveData()
    fun getDonut(): LiveData<List<DonutSection>> {
        donutSection.value = getDonutSections()
        return donutSection
    }

    private fun getDonutSections(): List<DonutSection> {
        return scaleView.updateSections(scaleView.calculateStrokes())
    }

    fun getUserHeight(): Float {
        val user = userRepository.getUser(USER_ID)
        return user.height.toFloat()
    }
    fun getUserGender() : String {
        val user = userRepository.getUser(USER_ID)
        return user.gender
    }
}