package com.emagrecebrasil.scaletestbank.ui

import android.app.AlertDialog
import android.app.Application
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.ui.signup.CadastroActivity
import com.emagrecebrasil.scaletestbank.util.ConfigScale
import com.emagrecebrasil.scaletestbank.util.LocalConfig
import com.emagrecebrasil.scaletestbank.util.SCALE_ADDRESS


class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ConfigScale.application = applicationContext as Application

        val navHostFragment = (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?)!!
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration.Builder(
            R.id.main_navigation
        ).build()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.config_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.user_config -> openConfig()
            R.id.reset_scale -> resetScale()
            R.id.config_close -> logoutUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun resetScale() {
        val savedLocal = LocalConfig(this)
        val address = savedLocal.getConfig(SCALE_ADDRESS)

        val alertDialog: AlertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Delete Scale")
        alertDialog.setMessage("Are you sure you want to delete the configured scale?\nCurrent address: $address")
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
            DialogInterface.OnClickListener { dialog, which ->
                savedLocal.deleteAll()
                dialog.dismiss() })
        alertDialog.show()
    }

    private fun openConfig() {
        val intent = Intent(this, CadastroActivity::class.java)
        startActivity(intent)
    }

    private fun logoutUser() {
        try {
            finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}