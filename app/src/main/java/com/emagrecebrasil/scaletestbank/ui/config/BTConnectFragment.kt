package com.gpm.emagrecebrasil.activity.ui.measurement.config

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.databinding.FragmentBtConnectBinding
import com.emagrecebrasil.scaletestbank.scale.ScaleManager
import kotlinx.coroutines.*
import senssun.blelib.model.BleDevice
import senssun.blelib.scan.BleScan
import senssun.blelib.scan.SSBleScanCallback
import java.util.*
import kotlin.properties.Delegates


class BTConnectFragment : Fragment() {

    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private lateinit var mBluetoothLeScanner: BluetoothLeScanner
    private lateinit var navController: NavController
    private lateinit var fragContext: Context
    private lateinit var toolbar: Toolbar
    private var neverScanned: Boolean = true
    private var mScanning: Boolean = false
    private var scaleDetected: Boolean = false

    private var deviceOK by Delegates.notNull<Boolean>()

    private var lvBles: ListView? = null
    private var bleScan: BleScan? = null
    private var adapter: BaseAdapter? = null
    private var devices: MutableList<BleDevice> = ArrayList()
    private lateinit var mTempAddr: String
    private lateinit var bleDevice: BleDevice
    private val jobScan = CoroutineScope(Dispatchers.IO)

    private val Fragment.packageManager get() = activity?.packageManager

    private var _binding: FragmentBtConnectBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        _binding = FragmentBtConnectBinding.inflate(inflater, container, false)
        val view = binding.root

        toolbar = binding.btConnectToolbar.toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val toolText = binding.btConnectToolbar.toolbarTitle
        toolText.setText(R.string.bt_connect_title)
        val toolEmpty = (activity as AppCompatActivity).supportActionBar
        toolEmpty!!.setDisplayShowTitleEnabled(false)

        binding.btConnectBluetoothAvailableImg.visibility = View.INVISIBLE
        binding.btConnectBluetoothActiveImg.visibility = View.INVISIBLE
        binding.progressDetect.visibility = View.INVISIBLE
        binding.txtWarn.visibility = View.INVISIBLE

        ScaleManager.init(requireContext())

        return view
    }


    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)


        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24)
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        binding.buttonVolta.visibility = View.INVISIBLE
        binding.buttonVolta.setOnClickListener {
            ScaleManager.getInstance().stopScan()
            startScaleScan()
        }

        val deviceHasBluetooth = configBluetooth()
        if (deviceHasBluetooth) {
            // Check the first message
            binding.btConnectBluetoothAvailableImg.visibility = View.VISIBLE
            binding.btConnectBluetoothAvailableImg.setImageResource(R.drawable.confirm)
            binding.btConnectBluetoothAvailableTxt.setTextColor(R.color.confirm_color)
            if (checkBluetooth()) {
                Log.d(TAG, "Iniciando scan de balanças Senssun")
                binding.btConnectBluetoothActiveImg.visibility = View.VISIBLE
                binding.btConnectBluetoothActiveImg.setImageResource(R.drawable.confirm)
                binding.btConnectBluetoothActiveTxt.setTextColor(R.color.confirm_color)
                if (neverScanned) {
                    startScaleScan()
                } else {
                    if (mScanning) {
                        ScaleManager.getInstance().stopScan()
                    }
                    Log.d(TAG, "Habilitando o rescan")
                    enableReScan()
                }
            } else {
                Log.d(TAG, "Bluetooth disponível mas desligado")
                binding.btConnectBluetoothActiveImg.visibility = View.VISIBLE
                binding.btConnectBluetoothActiveImg.setImageResource(R.drawable.cancel)
                binding.btConnectBluetoothActiveTxt.setTextColor(R.color.cancel_color)
            }
        } else {
            Log.d(TAG, "Bluetooth não disponível nesse aparelho")
            binding.btConnectBluetoothAvailableImg.visibility = View.VISIBLE
            binding.btConnectBluetoothAvailableImg.setImageResource(R.drawable.cancel)
            binding.btConnectBluetoothAvailableTxt.setTextColor(R.color.cancel_color)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        ScaleManager.getInstance().stopScan()
    }


    @SuppressLint("ServiceCast")
    fun configBluetooth(): Boolean {
        deviceOK = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            Log.d(TAG, "Verifica permissão")
            makeRequest()
        }

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!packageManager?.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)!!) {
            deviceOK = false
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        val bluetoothManager =
            requireActivity().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        Log.d(TAG, "mBluetooth adapter ativado:" + mBluetoothAdapter.isEnabled.toString())

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "Bluetooth não suportado nesse dispositivo")
            Toast.makeText(context, R.string.ble_not_supported, Toast.LENGTH_SHORT).show()
            deviceOK = false
        }
        Log.d(TAG, "ConfigBluetooth chamado com sucesso")
        return deviceOK
    }

    private fun startScaleScan() {
        binding.progressDetect.visibility = View.VISIBLE
        binding.txtWarn.visibility = View.VISIBLE
        binding.txtWarn.text = resources.getString(R.string.searchingScale)
        binding.buttonVolta.visibility = View.INVISIBLE

        ScaleManager.getInstance().scan(10000L, object : SSBleScanCallback() {
            override fun onScanResult(device: BleDevice) {
                mScanning = true
                jobScan.launch {
/*                    if (devices.contains(device)) {
                        devices[devices.indexOf(device)].rssi = device.rssi
                    } else {
                        devices.add(device)
                    }
                    Collections.sort(devices, BleDevice.Comparator)*/
                    withContext(Dispatchers.Main) {
                        neverScanned = false
                        mTempAddr = device.bluetoothDevice.address
                        val devName = device.bluetoothDevice.name
                        val manuf = device.manuData
                        val model = device.modelID
                        Log.d(TAG, "*********************************")
                        Log.d(TAG, "Dispositivo encontrado: $devName")
                        Log.d(TAG, "Endereço do dispositivo: $mTempAddr")
                        Log.d(TAG, "Manufacturer Data: $manuf")
                        Log.d(TAG, "Model ID da balança: $model")

                        val name = device.deviceType.BroadCasterName

                        if (name == "IF_B7") {
                            scaleDetected = true
                            Log.d(TAG, "===>>>>> Nome encontrado: $name")
                        }
                        if (scaleDetected) {
                            ScaleManager.getInstance().stopScan()
                            goToCheckDevice(device)
                        }
                    }


                }
            }

            override fun onScanStop(p0: MutableList<BleDevice>?) {
                enableReScan()
                mScanning = false
            }

            override fun onScanFail(p0: Int) {
                enableReScan()
            }
        })
    }

    private fun enableReScan() {
        binding.progressDetect.visibility = View.INVISIBLE
        binding.buttonVolta.visibility = View.VISIBLE
        binding.txtWarn.text = resources.getString(R.string.noScaleMsg)
        Log.d(TAG, "--->>> Pronto para rescanear")
    }

    private fun goToCheckDevice(bleDevice: BleDevice) {
        jobScan.cancel()
        val nextAction = BTConnectFragmentDirections.toBTDetectFragment(bleDevice)
        navController.navigate(nextAction)
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            PERMISSION_REQUEST_COARSE_LOCATION
        )
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.fragContext = context
    }

    fun setBluetoothAdapter(btAdapter: BluetoothAdapter) {
        this.mBluetoothAdapter = btAdapter
    }

    private fun checkBluetooth(): Boolean {
        var check: Boolean = false
        val bluetoothManager =
            activity?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        Log.d(TAG, "mBluetooth adapter ativado:" + mBluetoothAdapter.isEnabled.toString())

        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {

            // Is Bluetooth turned on?
            if (mBluetoothAdapter.isEnabled) {

                // Are Bluetooth Advertisements supported on this device?
                if (mBluetoothAdapter.isMultipleAdvertisementSupported) {

                    // Everything is supported and enabled, able to start scan.
                    mBluetoothLeScanner = mBluetoothAdapter.bluetoothLeScanner
                    check = true
                }
            }
        }
        return check
    }

    companion object {
        private val TAG = BTConnectFragment::class.java.simpleName
        private const val PERMISSION_REQUEST_COARSE_LOCATION = 1
    }
}