package com.emagrecebrasil.scaletestbank.ui

import android.annotation.SuppressLint
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import app.futured.donut.DonutProgressView
import app.futured.donut.DonutSection
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.databinding.MeasurementFragmentBinding
import com.emagrecebrasil.scaletestbank.scale.ScaleData
import com.emagrecebrasil.scaletestbank.scale.ScaleManager
import com.emagrecebrasil.scaletestbank.scale.ScaleView
import com.emagrecebrasil.scaletestbank.ui.config.BTConfigActivity
import com.emagrecebrasil.scaletestbank.ui.signup.CadastroActivity
import com.emagrecebrasil.scaletestbank.util.ConfigScale.bleDeviceAddress
import com.emagrecebrasil.scaletestbank.util.ConfigScale.bleDeviceAvailable
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import senssun.blelib.model.BleDevice
import senssun.blelib.model.WeightBean
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class MeasurementFragment : Fragment(), ScaleManager.OnScaleListener {

    private val scaleView: ScaleView by inject()
    private val scaleData: ScaleData by inject()
    private val mViewModel: MeasurementViewModel by viewModel()

    private var today: Date = Calendar.getInstance().time
    private var readyToSave: Boolean = true
    private var isStable: Boolean = false

    private lateinit var donutProgressView: DonutProgressView
    private lateinit var navController: NavController
    private lateinit var toolbar: Toolbar

    private lateinit var mFragmentManager: FragmentManager
    private var mInfoAddit = AdditionalData()

    //Format numbers
    private val numberFormat: String = "#.###,0"
    private val weightFormat: String = "##0,00"
    private val decimalFormat: DecimalFormat =
        NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat

    private var scaleDevice: BleDevice? = null
    private var bleAddress: String? = null

    private var _binding: MeasurementFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = MeasurementFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        toolbar = binding.measurementToolbar.toolbar

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val toolText = binding.measurementToolbar.toolbarTitle
        toolText.setText(R.string.title_measurement)
        val toolEmpty = (activity as AppCompatActivity).supportActionBar
        toolEmpty!!.setDisplayShowTitleEnabled(false)

        // Init scale
        ScaleManager.init(requireContext())

        mFragmentManager = requireActivity().supportFragmentManager

        donutProgressView = binding.donutView
        CoroutineScope(Dispatchers.Unconfined).launch {
            setupDonut()
        }
        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!mViewModel.checkExistUserTable()) {
            val intent = Intent(requireContext(), CadastroActivity::class.java)
            startActivity(intent)
        }

        clearDisplay()
        readyToSave = true
        navController = Navigation.findNavController(view)

        fabListener()

        // Bluetooth
        checkScale()
    }

    private fun checkScale() {
        bleAddress = bleDeviceAddress
        if (bleAddress.isNullOrEmpty()) {
            //navController.navigate(R.id.action_navigation_measurement_to_nav_BTConnect)
            val intent = Intent(requireContext(), BTConfigActivity::class.java)
            startActivity(intent)

        } else {
            CoroutineScope(Dispatchers.IO).launch {
                scaleDevice = bleDeviceAvailable
                Log.d(TAG, "Referencia da scale: $scaleDevice")
                if (scaleDevice != null) {
                    ScaleManager.getInstance().connect(scaleDevice)
                    ScaleManager.getInstance().addOnScaleListener(this@MeasurementFragment)
                    Log.d(
                        TAG,
                        "Endereço recuperado e cadastrado: ${scaleDevice!!.bluetoothDevice.address}"
                    )
                }
            }
        }
    }

    private fun fabListener() {
        binding.fabScale.setOnClickListener {
            clearDisplay()
            scaleView.clearStrokes()
            donutProgressView.submitData(scaleView.updateSections(scaleView.allStrokes))
            checkScale()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        clearDisplay()
        readyToSave = true
        isStable = false
    }

    override fun onDestroy() {
        super.onDestroy()
        ScaleManager.getInstance().removeOnScaleListener(this)
    }

    private fun setupDonut() {
        val totalTime = 1000L
        donutProgressView.cap = 180f
        donutProgressView.masterProgress = 180f
        donutProgressView.submitData(scaleView.updateSections(scaleView.fullStrokes))
        CoroutineScope(Dispatchers.Default).launch {
            delay(totalTime)
            withContext(Dispatchers.Main) {
                scaleView.clearStrokes()
                donutProgressView.submitData(scaleView.updateSections(scaleView.allStrokes))
            }
        }
    }

    private fun updateDonut(section: List<DonutSection>) {
        donutProgressView.cap = 180f
        donutProgressView.masterProgress = 180f
        donutProgressView.submitData(sections = section)
    }

    override fun onScaleState(state: Int) {
        if (state == BluetoothProfile.STATE_CONNECTED) {

            binding.fabScale.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    requireActivity().applicationContext,
                    R.color.confirm_color
                )
            )
            binding.scaleTextOn.setTextColor(
                ContextCompat.getColor(
                    requireActivity().applicationContext,
                    R.color.confirm_color
                )
            )
            binding.scaleTextOn.text = resources.getString(R.string.scale_connected)
            Log.d(TAG, "Balança conectada")
        } else {
            Log.d(TAG, "Balança desligada")
            readyToSave = true
            isStable = false
            binding.fabScale.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    requireActivity().applicationContext,
                    R.color.search_color
                )
            )
            binding.scaleTextOn.setTextColor(
                ContextCompat.getColor(
                    requireActivity().applicationContext,
                    R.color.search_color
                )
            )
            binding.scaleTextOn.text = resources.getString(R.string.scale_unconnected)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWeighting(weightBean: WeightBean) {
        val weight = weightBean.divisionWeightKG.toFloat()
        val impedance = weightBean.zuKang.toFloat()
        binding.textMeasurement.text = weight.toWeight()
        if (weightBean.isStable && !isStable) {
            CoroutineScope(Dispatchers.Default).launch {
                delay(5000L)
                withContext(Dispatchers.Main) {
                    Log.d(TAG, "Impedância medida: $impedance")
                    if (impedance == 0.0f && readyToSave) {
                        Toast.makeText(
                            context,
                            "Take your shoes or socks off and step up again",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
            isStable = true
        }
        if (impedance > 0.0 && readyToSave) {
            readyToSave = false
            binding.textMeasurement.setTypeface(null, Typeface.BOLD)
            Log.d(TAG, "Passou aqui, com impedancia = $impedance")
            scaleData.saveWeightInfo(weight, impedance, today)
            beepAndVibrate()
            startUpdateListeners()
        }
    }

    override fun onCommandReply(var1: ScaleManager.CommandType?, var2: String?) {

    }

    private fun clearDisplay() {
        binding.textMeasurement.setTypeface(null, Typeface.NORMAL)
        binding.textMeasurement.text = "0,0"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun beepAndVibrate() {
        val toneG = ToneGenerator(AudioManager.STREAM_ALARM, 100)
        (context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
            .vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE))
        toneG.startTone(ToneGenerator.TONE_DTMF_S, 150)
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            toneG.release()
        }, (200).toLong())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startUpdateListeners() {
        // Update additional data
        mViewModel.fetchAdditionalInfo().observe(viewLifecycleOwner, androidx.lifecycle.Observer { addit ->
            setAdditionalInfo(addit)
            mInfoAddit = addit
        })
        // Update the donut
        mViewModel.getDonut().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            it?.let { section ->
                updateDonut(section)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    fun setAdditionalInfo(addit: AdditionalData) {
        val kgUnit = resources.getString(R.string.unit_kg)
        val kcalUnit = resources.getString(R.string.unit_kcal)
        val percUnit = resources.getString(R.string.unit_perc)

        binding.txtBMI.text = addit.addBMI
        binding.txtResultBMI.text = addit.addBMIResult
        binding.txtResultBMI.background = getTintIMC(addit.addBMIResult)

        binding.txtBodyFat.text = "${addit.addBodyFat} $percUnit"
        binding.txtBodyFatW.text = "${addit.addBodyFatW} $kgUnit"
        binding.txtResultBodyFat.text = addit.addBodyFatResult
        binding.txtResultBodyFat.background = getTintBNAE(addit.addBodyFatResult)

        binding.txtWater.text = "${addit.addWater} $percUnit"
        binding.txtWaterW.text = "${addit.addWaterW} $kgUnit"
        binding.txtResultWater.text = addit.addWaterResult
        binding.txtResultWater.background = getTintBNA(addit.addWaterResult)

        binding.txtMuscleSkel.text = "${addit.addMuscleSkel} $percUnit"
        binding.txtMuscleSkelW.text = "${addit.addMuscleSkelW} $kgUnit"
        binding.txtResultMuscleSkel.text = addit.addMuscleSkelResult
        binding.txtResultMuscleSkel.background = getTintBNE(addit.addMuscleSkelResult)

        binding.txtBones.text = "${addit.addBones} $percUnit"
        binding.txtBonesW.text = "${addit.addBonesW} $kgUnit"
        binding.txtResultBones.text = addit.addBonesResult
        binding.txtResultBones.background = getTintBNA(addit.addBonesResult)

        binding.txtProtein.text = "${addit.addProtein} $percUnit"
        binding.txtProteinW.text = "${addit.addProteinW} $kgUnit"
        binding.txtResultProtein.text = addit.addProteinResult
        binding.txtResultProtein.background = getTintBNA(addit.addProteinResult)

        binding.txtMuscle.text = "${addit.addMuscle} $percUnit"
        binding.txtMuscleW.text = "${addit.addMuscleW} $kgUnit"
        binding.txtResultMuscle.text = addit.addMuscleResult
        binding.txtResultMuscle.background = getTintBNE(addit.addMuscleResult)

        binding.txtVfat.text = addit.addVfat
        binding.txtResultVfat.text = addit.addVfatResult
        binding.txtResultVfat.background = getTintIGV(addit.addVfatResult)

        binding.txtSubfat.text = "${addit.addSubFat} $kgUnit"
        binding.txtResultSubfat.text = addit.addSubFatResult
        binding.txtResultSubfat.background = getTintBNA(addit.addSubFatResult)

        binding.txtSlimfat.text = "${addit.addSlimFatW} $kgUnit"
        binding.txtResultSlimfat.text = addit.addSlimFatResult
        binding.txtResultSlimfat.background = getTintBNAE(addit.addSlimFatResult)

        binding.txtBMR.text = "${addit.addBMR} $kcalUnit"

        binding.txtAMR.text = "${addit.addAMR} $kcalUnit"

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun getTintBNA(result: String): Drawable? {
        var output: Drawable? = null
        when (result) {
            requireContext().getString(R.string.add_low) -> output =
                context?.getDrawable(R.drawable.bg_roundtext_yellow)
            requireContext().getString(R.string.add_normal) -> output =
                context?.getDrawable(R.drawable.bg_roundtext_green)
            requireContext().getString(R.string.add_high) -> output =
                context?.getDrawable(R.drawable.bg_rounded_red)
        }
        return output
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun getTintBNE(result: String): Drawable? {
        var output: Drawable? = null
        when (result) {
            requireContext().getString(R.string.add_low) -> output =
                context?.getDrawable(R.drawable.bg_rounded_blue)
            requireContext().getString(R.string.add_normal) -> output =
                context?.getDrawable(R.drawable.bg_roundtext_green)
            requireContext().getString(R.string.add_excel) -> output =
                context?.getDrawable(R.drawable.bg_rounded_darkblue)
        }
        return output
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun getTintIGV(result: String): Drawable? {
        var output: Drawable? = null
        when (result) {
            requireContext().getString(R.string.add_normal) -> output =
                context?.getDrawable(R.drawable.bg_roundtext_green)
            requireContext().getString(R.string.add_high) -> output =
                context?.getDrawable(R.drawable.bg_rounded_orange)
            requireContext().getString(R.string.add_very) -> output =
                context?.getDrawable(R.drawable.bg_rounded_red)
        }
        return output
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun getTintBNAE(result: String): Drawable? {
        var output: Drawable? = null
        when (result) {
            requireContext().getString(R.string.add_low) -> output =
                context?.getDrawable(R.drawable.bg_rounded_blue)
            requireContext().getString(R.string.add_normal) -> output =
                context?.getDrawable(R.drawable.bg_roundtext_green)
            requireContext().getString(R.string.add_high) -> output =
                context?.getDrawable(R.drawable.bg_rounded_orange)
            requireContext().getString(R.string.add_very) -> output =
                context?.getDrawable(R.drawable.bg_rounded_red)
        }
        return output
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun getTintIMC(result: String): Drawable? {
        var output: Drawable? = null
        when (result) {
            requireContext().getString(R.string.add_bodyshape_1) -> output =
                context?.getDrawable(R.drawable.bg_round_body_1)
            requireContext().getString(R.string.add_bodyshape_2) -> output =
                context?.getDrawable(R.drawable.bg_round_body_2)
            requireContext().getString(R.string.add_bodyshape_3) -> output =
                context?.getDrawable(R.drawable.bg_round_body_3)
            requireContext().getString(R.string.add_bodyshape_4) -> output =
                context?.getDrawable(R.drawable.bg_round_body_4)
            requireContext().getString(R.string.add_bodyshape_5) -> output =
                context?.getDrawable(R.drawable.bg_round_body_5)
            requireContext().getString(R.string.add_bodyshape_6) -> output =
                context?.getDrawable(R.drawable.bg_round_body_6)
            requireContext().getString(R.string.add_bodyshape_7) -> output =
                context?.getDrawable(R.drawable.bg_round_body_7)
            requireContext().getString(R.string.add_bodyshape_8) -> output =
                context?.getDrawable(R.drawable.bg_round_body_8)
        }
        return output
    }

    private fun Float.fixDec(): String {
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        decimalFormat.applyLocalizedPattern(numberFormat)
        return decimalFormat.format(this)
    }

    private fun Float.toWeight(): String {
        decimalFormat.applyLocalizedPattern(weightFormat)
        return decimalFormat.format(this)
    }


    companion object {
        private val TAG = MeasurementFragment::class.java.simpleName
    }

}