package com.emagrecebrasil.scaletestbank.di


import com.emagrecebrasil.scaletestbank.room.AppDatabase
import com.emagrecebrasil.scaletestbank.room.GraphRepo
import com.emagrecebrasil.scaletestbank.room.dao.UserDao
import com.emagrecebrasil.scaletestbank.room.dao.WeightDao
import com.emagrecebrasil.scaletestbank.room.datasource.UserDatabase
import com.emagrecebrasil.scaletestbank.room.datasource.WeightDatabase
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import com.emagrecebrasil.scaletestbank.scale.ScaleData
import com.emagrecebrasil.scaletestbank.scale.ScaleView
import com.emagrecebrasil.scaletestbank.ui.MeasurementViewModel
import com.emagrecebrasil.scaletestbank.ui.signup.CadastroViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val roomModule = module {

    single { AppDatabase.getDBInstance(androidApplication()) }

    fun provideUserDao(database: AppDatabase): UserDao {
        return database.userDao
    }

    fun provideWeightDao(database: AppDatabase): WeightDao {
        return database.weightDao
    }

    single { provideUserDao(get()) }
    single { provideWeightDao(get()) }
}

val repositoryModule = module {
    fun provideUserRepository(dao: UserDao): UserRepository {
        return UserDatabase(dao)
    }

    fun provideWeightRepository(dao: WeightDao): WeightRepository {
        return WeightDatabase(dao)
    }

    single { provideUserRepository(get()) }
    single { provideWeightRepository(get()) }
}

val measurementVMModule = module {
    factory {
        ScaleView(androidContext(), weightRepository = get(), userRepository = get())
    }

    factory {
        ScaleData(userRepository = get(), weightRepository = get())
    }

    factory {
        GraphRepo(weightRepository = get())
    }

    viewModel {
        MeasurementViewModel(weightRepository = get(), userRepository = get())
    }
}

val cadastroVMModule = module {
    viewModel {
        CadastroViewModel(userRepository = get())
    }
}



