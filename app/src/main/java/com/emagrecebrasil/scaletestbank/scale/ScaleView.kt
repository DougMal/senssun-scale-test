package com.emagrecebrasil.scaletestbank.scale

import android.content.Context
import android.util.Log
import app.futured.donut.DonutSection
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import com.emagrecebrasil.scaletestbank.util.*
import kotlin.math.pow

class ScaleView(
    val context: Context,
    val weightRepository: WeightRepository,
    val userRepository: UserRepository
) {


    companion object {
        const val START_VALUE = 22.5f
        const val EXTREME_LOW = 13.9f
        const val SEVERE_LOW = 16.9f
        const val LOW = 18.4f
        const val NORMAL = 24.9f
        const val OVER = 29.9f
        const val OBESITY_LOW = 34.9f
        const val OBESITY_HIGH = 39.9f
        private val All_Weights = listOf<DonutCategory>(
            ExtremeLowWeight,
            SevereLowWeight,
            LowWeight,
            NormalWeight,
            OverWeight,
            ObesityLowWeight,
            ObesityHighWeight,
            MorbidWeight
        )
        private val TAG = ScaleView::class.java.simpleName
    }

    inner class AllStrokes {
        var extremLowWeightStroke: Float = START_VALUE
        var severeLowWeightStroke: Float = START_VALUE
        var lowWeightStroke: Float = START_VALUE
        var normalWeightStroke: Float = START_VALUE
        var overWeightStroke: Float = START_VALUE
        var obesityLowWeightStroke: Float = START_VALUE
        var obesityHighWeightStroke: Float = START_VALUE
        var morbidWeightStroke: Float = START_VALUE

    }

    val fullStrokes = AllStrokes()
    val allStrokes = AllStrokes()

    private var weight: Float = 0.0f
    private var height: Int = 0

    fun updateSections(strokes: AllStrokes): List<DonutSection> {
        val section1 = DonutSection(
            name = ExtremeLowWeight.name,
            color = colorHex(ExtremeLowWeight.colorRes),
            amount = strokes.extremLowWeightStroke
        )
        val section2 = DonutSection(
            name = SevereLowWeight.name,
            color = colorHex(SevereLowWeight.colorRes),
            amount = strokes.severeLowWeightStroke
        )
        val section3 = DonutSection(
            name = LowWeight.name,
            color = colorHex(LowWeight.colorRes),
            amount = strokes.lowWeightStroke
        )
        val section4 = DonutSection(
            name = NormalWeight.name,
            color = colorHex(NormalWeight.colorRes),
            amount = strokes.normalWeightStroke
        )
        val section5 = DonutSection(
            name = OverWeight.name,
            color = colorHex(OverWeight.colorRes),
            amount = strokes.overWeightStroke
        )
        val section6 = DonutSection(
            name = ObesityLowWeight.name,
            color = colorHex(ObesityLowWeight.colorRes),
            amount = strokes.obesityLowWeightStroke
        )
        val section7 = DonutSection(
            name = ObesityHighWeight.name,
            color = colorHex(ObesityHighWeight.colorRes),
            amount = strokes.obesityHighWeightStroke
        )
        val section8 = DonutSection(
            name = MorbidWeight.name,
            color = colorHex(MorbidWeight.colorRes),
            amount = strokes.morbidWeightStroke
        )

        return listOf(
            section1,
            section2,
            section3,
            section4,
            section5,
            section6,
            section7,
            section8
        )
    }

    private fun colorHex(colorId: Int): Int {
        return context.resources!!.getColor(colorId)
    }

    fun calculateStrokes(): AllStrokes {
        val innerStrokes = AllStrokes()
        var heightCm: Float = 0.0f
        var mainBMI: Float = 0.0f
        var weightStep: Float = 0.0f
        var accWeight: Float = 0.0f

        val readHeight = userRepository.getUser(USER_ID)
        val readWeight = weightRepository.getLastWeight()

        weight = readWeight.dbWeight
        height = readHeight.height

        if (weight == null) {
            weight = 80.0f
        }
        //height = readWeight.
        if (height == null) {
            height = 175
        }

        // Check height unit
        heightCm = if (height > 100) {
            height.toFloat() / 100
        } else {
            height.toFloat()
        }

        // Calculate main BMI
        mainBMI = weight / heightCm.pow(2)

        // Check each range of BMI to define the strokes
        // Lower range is always available
        weightStep = EXTREME_LOW * heightCm.pow(2)
        if (weight >= weightStep) {
            innerStrokes.extremLowWeightStroke = weightStep
        } else {
            innerStrokes.extremLowWeightStroke = weight
        }
        accWeight += weightStep


        // Check range for severe weight
        if ((mainBMI > EXTREME_LOW) and (weight > accWeight)) {
            weightStep = SEVERE_LOW * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.severeLowWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.severeLowWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.severeLowWeightStroke = 0.0f
        }

        // Check range for low weight
        if ((mainBMI > SEVERE_LOW) and (weight > accWeight)) {
            weightStep = LOW * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.lowWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.lowWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.lowWeightStroke = 0.0f
        }

        // Check range for normal weight
        if ((mainBMI > LOW) and (weight > accWeight)) {
            weightStep = NORMAL * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.normalWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.normalWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.normalWeightStroke = 0.0f
        }

        // Check range for over weight
        if ((mainBMI > NORMAL) and (weight > accWeight)) {
            weightStep = OVER * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.overWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.overWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.overWeightStroke = 0.0f
        }

        // Check range for obesity lower weight
        if ((mainBMI > OVER) and (weight > accWeight)) {
            weightStep = OBESITY_LOW * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.obesityLowWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.obesityLowWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.obesityLowWeightStroke = 0.0f
        }

        // Check range for obesity higher weight
        if ((mainBMI > OBESITY_LOW) and (weight > accWeight)) {
            weightStep = OBESITY_HIGH * heightCm.pow(2)
            if (weight >= weightStep) {
                innerStrokes.obesityHighWeightStroke = weightStep - accWeight
            } else {
                innerStrokes.obesityHighWeightStroke = weight - accWeight
            }
            accWeight += (weightStep - accWeight)
        } else {
            innerStrokes.obesityHighWeightStroke = 0.0f
        }

        // Check range for morbid weight
        if ((mainBMI > OBESITY_HIGH) and (weight > accWeight)) {
            weightStep = (OBESITY_HIGH + 0.1f) * heightCm.pow(2)
            innerStrokes.morbidWeightStroke = weight - accWeight
        } else {
            innerStrokes.morbidWeightStroke = 0.0f
        }

        return innerStrokes
    }


    fun setInitialData(index: Int, direction: Boolean): List<DonutSection> {
        when (index) {
            0 -> clearStrokes()
            1 -> {
                if (direction) {
                    allStrokes.extremLowWeightStroke = START_VALUE
                } else {
                    allStrokes.extremLowWeightStroke = 0f
                }

            }
            2 -> {
                if (direction) {
                    allStrokes.severeLowWeightStroke = START_VALUE
                } else {
                    allStrokes.severeLowWeightStroke = 0f
                }

            }
            3 -> {
                if (direction) {
                    allStrokes.lowWeightStroke = START_VALUE
                } else {
                    allStrokes.lowWeightStroke = 0f
                }

            }
            4 -> {
                if (direction) {
                    allStrokes.normalWeightStroke = START_VALUE
                } else {
                    allStrokes.normalWeightStroke = 0f
                }

            }
            5 -> {
                if (direction) {
                    allStrokes.overWeightStroke = START_VALUE
                } else {
                    allStrokes.overWeightStroke = 0f
                }

            }
            6 -> {
                if (direction) {
                    allStrokes.obesityLowWeightStroke = START_VALUE
                } else {
                    allStrokes.obesityLowWeightStroke = 0f
                }

            }
            7 -> {
                if (direction) {
                    allStrokes.obesityHighWeightStroke = START_VALUE
                } else {
                    allStrokes.obesityHighWeightStroke = 0f
                }

            }
            8 -> {
                if (direction) {
                    allStrokes.morbidWeightStroke = START_VALUE
                } else {
                    allStrokes.morbidWeightStroke = 0f
                }

            }
        }
        printStroke(index)
        return updateSections(allStrokes)
    }

    fun clearStrokes() {
        allStrokes.morbidWeightStroke = 0f
        allStrokes.obesityHighWeightStroke = 0f
        allStrokes.obesityLowWeightStroke = 0f
        allStrokes.overWeightStroke = 0f
        allStrokes.normalWeightStroke = 0f
        allStrokes.lowWeightStroke = 0f
        allStrokes.severeLowWeightStroke = 0f
        allStrokes.extremLowWeightStroke = 0f
    }

    private fun printStroke(index: Int) {
        Log.d(TAG, "-------------------------------------------------")
        Log.d(TAG, "Sections $index: Extreme Low = ${allStrokes.extremLowWeightStroke}")
        Log.d(TAG, "Sections $index: Severe Low = ${allStrokes.severeLowWeightStroke}")
        Log.d(TAG, "Sections $index: Low = ${allStrokes.lowWeightStroke}")
        Log.d(TAG, "Sections $index: Normal = ${allStrokes.normalWeightStroke}")
        Log.d(TAG, "Sections $index: Over = ${allStrokes.overWeightStroke}")
        Log.d(TAG, "Sections $index: Obesity Low = ${allStrokes.obesityLowWeightStroke}")
        Log.d(TAG, "Sections $index: Obesity High = ${allStrokes.obesityHighWeightStroke}")
        Log.d(TAG, "Sections $index: Morbid = ${allStrokes.morbidWeightStroke}")
    }

}