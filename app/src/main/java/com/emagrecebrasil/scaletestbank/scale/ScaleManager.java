package com.emagrecebrasil.scaletestbank.scale;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import senssun.blelib.device.scale.cloudblelib.BleCloudProtocolUtils;
import senssun.blelib.device.scale.cloudblelib.BleCloudProtocolUtils.OnAllUsers;
import senssun.blelib.device.scale.cloudblelib.BleCloudProtocolUtils.OnConnectState;
import senssun.blelib.device.scale.cloudblelib.BleCloudProtocolUtils.OnDisplayDATA;
import senssun.blelib.device.scale.cloudbroadcastlib.BroadCastCloudProtocolUtils;
import senssun.blelib.device.scale.fatblelib.BleFatProtocolUtils;
import senssun.blelib.device.scale.qihoo.QiHooProtocolUtils;
import senssun.blelib.model.BleDevice;
import senssun.blelib.model.BleDevice.DeviceType;
import senssun.blelib.model.SSFatBean;
import senssun.blelib.model.SysUserInfo;
import senssun.blelib.model.WeightBean;
import senssun.blelib.model.e;
import senssun.blelib.scan.BleScan;
import senssun.blelib.scan.SSBleScanCallback;


public class ScaleManager {
    
    private static ScaleManager mScaleManager;
    private Context a;
    private List<OnScaleListener> b = new ArrayList();
    private BleDevice c;
    private Handler d = new Handler(Looper.getMainLooper());
    private DeviceType e;
    private long f;
    private Runnable g = new Runnable() {
        public void run() {
            BroadCastCloudProtocolUtils.getInstance().openBroadcast();
        }
    };

    public long getRecordTime() {
        return this.f;
    }

    private ScaleManager(Context var1) {
        this.a = var1;
        BleCloudProtocolUtils.getInstance().init(var1);
        BroadCastCloudProtocolUtils.getInstance().init(var1);
        BleFatProtocolUtils.getInstance().init(var1);
        QiHooProtocolUtils.getInstance(var1);
        this.c(var1);
        this.a(var1);
        this.b(var1);
    }

    private void a(Context var1) {
        BleCloudProtocolUtils.getInstance().setOnDisplayDATA(new OnDisplayDATA() {
            public void OnDATA(JSONObject var1) {
                String var2 = var1.getString("Type");
                CommandType var3 = null;
                if (CommandType.DATA_SYSC.key.contains(var2)) {
                    var3 = CommandType.DATA_SYSC;
                } else if (CommandType.WEIGHT_FINISH.key.contains(var2)) {
                    var3 = CommandType.WEIGHT_FINISH;
                } else if (CommandType.USER_QUERY_ALL.key.contains(var2)) {
                    var3 = CommandType.USER_QUERY_ALL;
                }

                Iterator var4 = ScaleManager.this.b.iterator();

                while(var4.hasNext()) {
                    OnScaleListener var5 = (OnScaleListener)var4.next();
                    var5.onCommandReply(var3, var1.toJSONString());
                }

            }
        });
    }

    private void b(Context var1) {
        BleCloudProtocolUtils.getInstance().setOnDisplayDATA(new OnDisplayDATA() {
            public void OnDATA(JSONObject var1) {
                String var2 = var1.getString("Type");
                if (!"89".equals(var2) && !"82".equals(var2)) {
                    ScaleManager.this.a(var1, 0);
                } else {
                    ScaleManager.this.a(var1, 2);
                }

            }
        });
        BroadCastCloudProtocolUtils.getInstance().setOnDisplayDATA(new BroadCastCloudProtocolUtils.OnDisplayDATA() {
            public void OnDATA(JSONObject var1) {
                ScaleManager.this.a(var1, 1);
            }
        });
        BleFatProtocolUtils.getInstance().setOnDisplayDATA(new BleFatProtocolUtils.OnDisplayDATA() {
            public void OnDATA(e var1) {
            }
        });
        QiHooProtocolUtils.getInstance(var1).setOnDisplayDATA(new QiHooProtocolUtils.OnDisplayDATA() {
            public void OnDATA(String var1, String var2) {
            }
        });
    }

    private void a(JSONObject var1, int var2) {
        SSFatBean var3 = new SSFatBean();
        boolean var4 = var1.getBoolean("ifStable") == null ? false : var1.getBoolean("ifStable");
        Integer var5 = var1.getInteger("weight") == null ? 0 : var1.getInteger("weight");
        Integer var6 = var1.getInteger("lbWeight") == null ? 0 : var1.getInteger("lbWeight");
        Integer var7 = TextUtils.isEmpty(var1.getString("unit")) ? 0 : Integer.valueOf(var1.getString("unit"));
        Integer var8 = TextUtils.isEmpty(var1.getString("division")) ? 0 : Integer.valueOf(var1.getString("division"));
        Integer var9 = TextUtils.isEmpty(var1.getString("zuKang")) ? 0 : Integer.valueOf(var1.getString("zuKang"));
        String var10 = TextUtils.isEmpty(var1.getString("address")) ? null : var1.getString("address");
        WeightBean var11 = new WeightBean(var5, var7, var8, var6, var9, var4);
        if (this.c != null) {
            var11.setAddress(this.c.getBluetoothDevice().getAddress());
        } else {
            var11.setAddress(var10);
        }

        if (var4) {
            var11.setDataType(WeightBean.a.a);
        }

        if (var2 == 2) {
            var11.setDataType(WeightBean.a.b);
            var11.setStable(true);
            int var12 = var1.getInteger("leftHandImpedance") == null ? 0 : var1.getInteger("leftHandImpedance");
            int var13 = var1.getInteger("rightHandImpedance") == null ? 0 : var1.getInteger("rightHandImpedance");
            int var14 = var1.getInteger("leftFootImpedance") == null ? 0 : var1.getInteger("leftFootImpedance");
            int var15 = var1.getInteger("rightFootImpedance") == null ? 0 : var1.getInteger("rightFootImpedance");
            int var16 = var1.getInteger("trunkImpedance") == null ? 0 : var1.getInteger("trunkImpedance");
            int var17 = var1.getInteger("fourImpedance") == null ? 0 : var1.getInteger("fourImpedance");
            long var18 = var1.getLong("dateTime") == null ? 0L : var1.getLong("dateTime");
            int var20 = TextUtils.isEmpty(var1.getString("result")) ? 0 : Integer.valueOf(var1.getString("result"));
            String var21 = TextUtils.isEmpty(var1.getString("pin")) ? null : var1.getString("pin");
            String var22 = TextUtils.isEmpty(var1.getString("sys")) ? null : var1.getString("sys");
            int var23 = var1.getInteger("fat") == null ? 0 : var1.getInteger("fat");
            int var24 = var1.getInteger("moisture") == null ? 0 : var1.getInteger("moisture");
            int var25 = var1.getInteger("muscles") == null ? 0 : var1.getInteger("muscles");
            int var26 = var1.getInteger("bone") == null ? 0 : var1.getInteger("bone");
            int var27 = var1.getInteger("BMR") == null ? 0 : var1.getInteger("BMR");
            int var28 = var1.getInteger("heartRate") == null ? 0 : var1.getInteger("heartRate");
            var3.setMoisture((float)var24);
            var3.setMuscles((float)var25);
            var3.setBone((float)var26);
            var3.setBMR((float)var27);
            var3.setHeartRate((float)var28);
            var3.setFat((float)var23);
            var3.setSys(var22);
            var3.setPin(var21);
            var3.setResult(var20);
            var3.setDateTime(var18);
            var3.setLeftHandImpedance(var12);
            var3.setRightHandImpedance(var13);
            var3.setLeftFootImpedance(var14);
            var3.setRightFootImpedance(var15);
            var3.setTrunkImpedance(var16);
            var3.setFourImpedance(var17);
        }

        var11.setSsFatBean(var3);
        if (var2 == 1) {
            var11.setDivision(var11.getDivision() - 1);
            if (var11.getDivision() <= 0) {
                var11.setWeightKG(var11.getWeightKG() / 10.0F);
            }
        }

        try {
            Iterator var30;
            OnScaleListener var31;
            if ((var9 <= 0 || !var4) && (!var11.isStable() || var11.getDataType() != WeightBean.a.b)) {
                var30 = this.b.iterator();

                while(var30.hasNext()) {
                    var31 = (OnScaleListener)var30.next();
                    var31.onWeighting(var11);
                }
            } else if (System.currentTimeMillis() - this.f > 10000L) {
                this.f = System.currentTimeMillis();
                var11.setDataType(WeightBean.a.b);
                var30 = this.b.iterator();

                while(var30.hasNext()) {
                    var31 = (OnScaleListener)var30.next();
                    var31.onWeighting(var11);
                }
            }
        } catch (Exception var29) {
            var29.printStackTrace();
        }

    }

    private void c(Context var1) {
        BleCloudProtocolUtils.getInstance().setOnConnectState(new OnConnectState() {
            public void OnState(boolean var1) {
                ScaleManager.this.a(var1);
            }
        });
        BleFatProtocolUtils.getInstance().setOnConnectState(new BleFatProtocolUtils.OnConnectState() {
            public void OnState(boolean var1) {
                ScaleManager.this.a(var1);
            }
        });
        BroadCastCloudProtocolUtils.getInstance().setOnConnectState(new BroadCastCloudProtocolUtils.OnConnectState() {
            public void OnState(boolean var1) {
                ScaleManager.this.a(var1);
            }
        });
        QiHooProtocolUtils.getInstance(var1).setOnConnectState(new QiHooProtocolUtils.OnConnectState() {
            public void OnState(boolean var1) {
                ScaleManager.this.a(var1);
            }
        });
    }

    private void a(boolean var1) {
        int var2 = var1 ? 2 : 0;

        try {
            Iterator var3 = this.b.iterator();

            while(var3.hasNext()) {
                OnScaleListener var4 = (OnScaleListener)var3.next();
                var4.onScaleState(var2);
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    
    public static void init(Context var0) {
        if (mScaleManager == null) {
            mScaleManager = new ScaleManager(var0);
        }

    }

    public void onUserCallback() {
        BleCloudProtocolUtils.getInstance().setOnAllUsers(new OnAllUsers() {
            public void OnShow(ArrayList<SysUserInfo> var1, boolean var2) {
            }
        });
    }

    
    public static ScaleManager getInstance() {
        return mScaleManager;
    }

    public void scan(long var1, SSBleScanCallback var3) {
        BleScan.getInstance().scanStartDevice(var1, var3);
    }

    public void stopScan() {
        BleScan.getInstance().scanStopDevice();
    }

    public void openBroadcast() {
        if (this.d != null) {
            this.d.removeCallbacks(this.g);
            this.d.postDelayed(this.g, 2000L);
        }

    }

    public void closeBroadcast() {
        if (this.d != null) {
            this.d.removeCallbacks(this.g);
        }

        BroadCastCloudProtocolUtils.getInstance().Disconnect();
    }

    
    public void addUser(String var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
                BleCloudProtocolUtils.getInstance().AddUserInfo(var1, var2, var3, var4, var5, var6, var7);
                break;
            case BLE_OLD:
                BleFatProtocolUtils.getInstance().SendTestFatInfo(var3, var4, var2, Integer.valueOf(var1));
        }

    }

    public ScaleManager setBroadcast(DeviceType var1) {
        this.e = var1;
        BroadCastCloudProtocolUtils.getInstance().setDeviceType(this.e);
        return this;
    }

    public ScaleManager addDeviceType(DeviceType var1) {
        this.e = var1;
        BroadCastCloudProtocolUtils.getInstance().addDeviceType(this.e);
        return this;
    }

    
    public void connect(BleDevice var1) {
        this.c = var1;
        if (var1.getProtocolType() != null) {
            switch(var1.getProtocolType()) {
                case BLE_DEFAULT:
                    BleCloudProtocolUtils.getInstance().Connect(var1.getBluetoothDevice().getAddress());
                    break;
                case BROAD_DEFAULT:
                    BroadCastCloudProtocolUtils.getInstance().Connect(var1.getBluetoothDevice().getAddress());
            }

        }
    }

    private boolean a(BleDevice var1) {
        return "8001".equals(var1.getModelID());
    }

    
    public void disconnect() {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
                BleCloudProtocolUtils.getInstance().Disconnect();
                break;
            case BLE_OLD:
                BleFatProtocolUtils.getInstance().Disconnect();
                break;
            case BROAD_DEFAULT:
                BroadCastCloudProtocolUtils.getInstance().Disconnect();
                break;
            case BLE_QiHoo:
                QiHooProtocolUtils.getInstance(this.a).Disconnect();
        }

    }

    public boolean isConnect() {
        return BleCloudProtocolUtils.getInstance().isConnect() == 2 || BroadCastCloudProtocolUtils.getInstance().isConnect() == 2;
    }

    
    public void sycHistoryData(String var1, boolean var2) {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
                if (this.c.getDeviceType() == DeviceType.BleFatSuperScale2) {
                    if (var2) {
                        BleCloudProtocolUtils.getInstance().SendDataCommunAll2(var1);
                    } else {
                        BleCloudProtocolUtils.getInstance().SendDataCommun2(var1);
                    }
                } else if (var2) {
                    BleCloudProtocolUtils.getInstance().SendDataCommunAll(var1);
                } else {
                    BleCloudProtocolUtils.getInstance().SendDataCommun(var1);
                }
                break;
            case BLE_OLD:
                if (var2) {
                    BleFatProtocolUtils.getInstance().SendDataCommun(Integer.valueOf(var1));
                } else {
                    BleFatProtocolUtils.getInstance().SendDataCommun(Integer.valueOf(var1));
                }
            case BROAD_DEFAULT:
            case BLE_QiHoo:
        }

    }

    
    public void queryAllUser() {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
                BleCloudProtocolUtils.getInstance().QueryAllUserInfo();
            default:
        }
    }

    
    public void deleteUser(String var1) {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
                BleCloudProtocolUtils.getInstance().DelUserInfo(var1);
            case BLE_OLD:
            default:
        }
    }

    
    public void resetScale() {
        switch(this.c.getProtocolType()) {
            case BLE_DEFAULT:
            case BLE_QiHoo:
                BleCloudProtocolUtils.getInstance().ResetBuffer();
            case BLE_OLD:
            case BROAD_DEFAULT:
            default:
        }
    }

    
    public void addDeviceListener(senssun.blelib.device.scale.cloudblelib.a var1) {
        BleCloudProtocolUtils.getInstance().getRealConnector().addDeviceListener(var1);
    }

    
    public void removeDeviceListener(senssun.blelib.device.scale.cloudblelib.a var1) {
        BleCloudProtocolUtils.getInstance().getRealConnector().removeDeviceListener(var1);
    }

    
    public void addOnScaleListener(OnScaleListener var1) {
        this.b.add(var1);
    }

    
    public void removeOnScaleListener(OnScaleListener var1) {
        this.b.remove(var1);
    }

    public interface onBroadcastScaleListener {
        
        void onWeighting(WeightBean var1);
    }

    
    public interface OnScaleListener {
        
        void onScaleState(int var1);

        
        void onWeighting(WeightBean var1);

        
        void onCommandReply(CommandType var1, String var2);
    }

    
    public static enum CommandType {
        
        DATA_SYSC(1, "81"),
        
        USER_QUERY_ALL(2, "87"),
        
        WEIGHT_FINISH(3, "82");

        
        public String key;
        
        public int id;

        private CommandType(int var3, String var4) {
            this.key = var4;
            this.id = this.id;
        }
    }
}

