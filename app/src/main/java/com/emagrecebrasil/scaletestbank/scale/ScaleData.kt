package com.emagrecebrasil.scaletestbank.scale

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import com.emagrecebrasil.scaletestbank.room.entity.User
import com.emagrecebrasil.scaletestbank.room.entity.Weight
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import com.emagrecebrasil.scaletestbank.util.USER_ID
import com.xshq.spring.utils.CouStru
import org.koin.core.KoinComponent
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.util.*


class ScaleData(
    val userRepository: UserRepository,
    val weightRepository: WeightRepository
) :
    KoinComponent {

    private val senssunCalculated = SenssunCalculated()
    private lateinit var weightInfo: Weight
    private lateinit var userInfo: User


    @RequiresApi(Build.VERSION_CODES.O)
    fun saveWeightInfo(weight: Float, impedance: Float, data: Date) {

        if (userRepository.isTableExists()) {
            userInfo = getUserInfo(USER_ID)
        }
        val scaleCalc = calcBodyInfoSS(weight, impedance)

        val dateTime = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat.getDateInstance()
        val timeFormat = SimpleDateFormat.getTimeInstance()
        val time = timeFormat.format(dateTime).toString()

        weightInfo = Weight(
            dbUserOwnerId = userInfo.userId,
            dbDate = data, //dateTime as Date?,
            dbTime = time,
            dbWeight = weight,
            dbImpedance = impedance,
            dbBMI = scaleCalc.dtBMI,
            dbVFat = scaleCalc.dtVFat,
            dbFat = scaleCalc.dtFat,
            dbFatW = scaleCalc.dtFatW,
            dbSlim = scaleCalc.dtSlim,
            dbSlimW = scaleCalc.dtSlimW,
            dbSubFat = scaleCalc.dtSubFat,
            dbSubFatW = scaleCalc.dtSubFatW,
            dbMoisture = scaleCalc.dtMoisture,
            dbMoistureW = scaleCalc.dtMoistureW,
            dbMuscle = scaleCalc.dtMuscle,
            dbMuscleW = scaleCalc.dtMuscleW,
            dbBone = scaleCalc.dtBone,
            dbBoneW = scaleCalc.dtBoneW,
            dbProtein = scaleCalc.dtProtein,
            dbProteinW = scaleCalc.dtProteinW,
            dbSkeletalMuscle = scaleCalc.dtSkeletalMuscle,

            dbSkeletalMuscleW = scaleCalc.dtSkeletalMuscleW,
            dbBodyScore = scaleCalc.dtBodyScore,
            dbBodyAge = scaleCalc.dtBodyAge,
            dbBodyType = scaleCalc.dtBodyType,
            dbBMR = scaleCalc.dtBMR,
            dbAMR = scaleCalc.dtAMR
        )

        // Save into local database
        val weightCount = weightRepository.insertWeight(weightInfo)
    }

    /**
     * Calculation from Senssum SDK - Camry
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun calcBodyInfoSS(weight: Float, impedance: Float): SenssunCalculated {
        val bDayStr = userInfo.birthday
        val genderStr = userInfo.gender
        val height = userInfo.height.toFloat()
        val activity = userInfo.activityLevel
        val ac = 1

        // Check if gender is male or female
        val gender: Int = if (genderStr == "Female") {
            0
        } else {
            1
        }
        // Calculate user age based on BDay
        val age: Int = bDayStr.let { getAge(it).toFloat() }.toInt()

        // Calculate Body Info
        senssunCalculated.apply {
            dtBMI = CouStru.CountGetbmi(weight, height)
            dtVFat = CouStru.CountGetVisceralFat3(impedance, weight, height, age, gender, ac)
            dtFat = CouStru.CountGetFat3(impedance, weight, height, age, gender, ac)
            dtFatW = CouStru.CountGetFatWeight3(impedance, weight, height, age, gender, ac)
            dtSlim = 100 - dtFat
            dtSlimW = weight - dtFatW
            dtSubFat = CouStru.CountGetSubFat3(impedance, weight, height, age, gender, ac)
            dtSubFatW = CouStru.CountGetSubFatWeight3(impedance, weight, height, age, gender, ac)
            dtMoisture = CouStru.CountGetMoisture3(impedance, weight, height, age, gender, ac)
            dtMoistureW =
                CouStru.CountGetMoistureWeight3(impedance, weight, height, age, gender, ac)
            dtMuscle = CouStru.CountGetMuscle3(impedance, weight, height, age, gender, ac)
            dtMuscleW = CouStru.CountGetMuscleMass3(impedance, weight, height, age, gender, ac)
            dtBone = CouStru.CountGetBoneMass3(impedance, weight, height, age, gender, ac)
            dtBoneW = CouStru.CountGetBoneMassWeight3(impedance, weight, height, age, gender, ac)
            dtProtein = CouStru.CountGetProtein3(impedance, weight, height, age, gender, ac)
            dtProteinW = CouStru.CountGetProteinWeight3(impedance, weight, height, age, gender, ac)
            dtSkeletalMuscle =
                CouStru.CountGetSkeletalMuscle3(impedance, weight, height, age, gender, ac)
            dtSkeletalMuscleW =
                CouStru.CountGetSkeletalMuscleWeight3(impedance, weight, height, age, gender, ac)
            dtBodyScore = CouStru.CountGetBodyScore3(impedance, weight, height, age, gender, ac)
            dtBodyAge =
                CouStru.CountGetBodyAge3(impedance, weight, height, age, gender, activity, ac)
            dtBodyType = CouStru.CountGetBodyType3(impedance, weight, height, age, gender, ac)
            dtBMR = CouStru.CountGetBMR3(impedance, weight, height, age, gender, ac)
            dtAMR = CouStru.CountGetAMR3(impedance, weight, height, age, gender, activity, ac)
        }
        return senssunCalculated
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SimpleDateFormat")
    fun getAge(strDate: String): Int {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val bDay = Calendar.getInstance()
        bDay.time = sdf.parse(strDate)!!
        return Period.between(
            LocalDate.of(
                bDay.get(Calendar.YEAR),
                (bDay.get(Calendar.MONTH) + 1),
                bDay.get(Calendar.DAY_OF_MONTH)
            ),
            LocalDate.now()
        ).years
    }

    private fun getUserInfo(userId: String): User {
        return userRepository.getUser(userId)
    }

    companion object {
        private val TAG = ScaleData::class.java.simpleName
    }
}