package com.emagrecebrasil.scaletestbank.scale

data class SenssunCalculated(
        // Data structure for Senssun scale algorithm calculation
        var dtBMI: Float = 0f,          // Body Mass Index
        var dtVFat: Float = 0f,         // Visceral Fat
        var dtFat: Float = 0f,          // Body Fat - %
        var dtFatW: Float = 0f,         // Body Fat - kg
        var dtSubFat: Float = 0f,       // Subcutaneal Fat - %
        var dtSubFatW: Float = 0f,      // Subcutaneal Fat - kg
        var dtMoisture: Float = 0f,     // Water - %
        var dtMoistureW: Float = 0f,    // Water - kg
        var dtMuscle: Float = 0f,       // Muscle - %
        var dtMuscleW: Float = 0f,      // Muscle - kg
        var dtBone: Float = 0f,         // Bones - %
        var dtBoneW: Float = 0f,        // Bones - kg
        var dtProtein: Float = 0f,      // Protein - %
        var dtProteinW: Float = 0f,     // Protein - kg
        var dtSkeletalMuscle: Float = 0f,   // Skeletal Muscle - %
        var dtSkeletalMuscleW: Float = 0f,  // Skeletal Muscle - kg
        // Calculations
        var dtBodyScore: Float = 0f,    // Body Score
        var dtBodyAge: Float = 0f,      // Body Age
        var dtBodyType: Float = 0f,     // Body Type
        var dtBMR: Float = 0f,          // Basal Metabolism Rate - kcal
        var dtAMR: Float = 0f,          // Active Metabolism Rate - kcal
        var dtSlim: Float = 0f,         // Slim body - %
        var dtSlimW: Float = 0f         // Slim body - kg
)
