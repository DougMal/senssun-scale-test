package com.emagrecebrasil.scaletestbank.util

import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

/**
 * Database Room
 */
const val DATABASE_NAME = "emagrece_brasil_test_db"
const val USER_ID = "test_id"

const val SECONDS = 24 * 60 * 60
const val GMT3 = 3 * 60 * 60

/**
 * Scale local variable
 */
const val SCALE_OBJECT = "scale_object"
const val SCALE_NAME = "scale_name"
const val SCALE_ADDRESS = "scale_address"
const val SCALE_EB = "Balança Emagrece Brasil"



/**
 * Firebase
 */
// Storage path
const val STORAGE_PATH = "gs://emagrece-brasil.appspot.com"

// Feed constants
const val FEED_COLLECTION = "feed"
const val IMAGE_URL = "imageUrl"
const val POST_LIKES = "postLikes"
const val POST_COMMENTS = "postComments"
const val POST_SUBJECT = "postSubject"
const val POST_TITLE = "postTitle"
const val POST_TEXT = "postTxt"

// Chat constants
const val CHAT_COLLECTION = "chat"

// Promo constants
const val PROMO_COLLECTION = "promo"
const val PROMO_TIME_IN_MILLIS = 5000

// Content constants
const val CONTENT_COLLECTION = "content"
const val CONTENT_SEQUENCE = "contentSeq"

// Users constants
const val USER_COLLECTION = "user"
const val OK = "OK"
const val CONTRACT = "contract"
const val CARD = "card"
const val SUBSCRIPTION_INFO = "subscription"
const val MESSAGES_STORAGE = "messages"
const val PROFILE_STORAGE = "profile"
const val FCM = "fcm"
const val FCM_TOKEN = "fcmToken"
const val FCM_TOPIC = "fcmTopic"

// Diet constants
const val DIET_COLLECTION = "diet"
const val LOW_CARB = "low_carb"
const val KETOGENIC = "ketogenic"
const val NO_GLUTEN = "no_gluten"
const val NO_LACTOSE = "no_lactose"
const val MEDITERRANEAN = "mediterranean"
const val VEGETARIAN = "vegetarian"
const val HEADER = "header"
const val MEAL = "meal"
const val DIET_DESCRIPTION = "dietDescription"
const val BREAKFAST = "breakfast"
const val BRUNCH = "brunch"
const val DINNER = "dinner"
const val LUNCH = "lunch"
const val SNACK = "snack"
const val SUPPER = "supper"
const val DIET_OPTION = "options"
const val DIET_LIST = "diet_list"
const val KCAL1200 = "kcal_1200"
const val KCAL1400 = "kcal_1400"
const val KCAL1600 = "kcal_1600"
const val DAY = "optionSequence"
const val NUTRITIONAL = "optionNutritional"

// Program constants
const val PROGRAM_COLLECTION = "program"
const val PRG_LIST = "program_list"
const val PRG_DESCRIPTION = "prgDescription"

// Video constants
const val VIDEO_COLLECTION = "video"
const val VIDEO_INFO = "info"
const val VIDEO_EXERCISE = "exercise"
const val VIDEO_RECIPE = "recipe"
const val VIDEO_TYPE = "video_type"
const val VIDEO_TYPE_FIELD = "videoIcon"

// Weight constants
const val WEIGHT_COLLECTION = "weight"

// Store constants
const val STORE_COLLECTION = "store"
const val ITEMS = "items"
const val SERVICE = "service"
const val PRODUCT = "product"

// Recipe constants
const val RECIPE_COLLECTION = "recipe"
const val RECIPE_TYPE = "recipe_type"
const val REC_TYPE_FIELD = "recipeType"
const val REC_NUTRI = "recipeNutritional"
const val REC_DIET = "recipeDiet"

// Policy constants
const val POLICY_FOLDER = "policy"
const val TERMS_USE_FILE = "terms_of_use.html"
const val PRIVACY_POLICY_FILE = "privacy_policy.html"

// Coupons
const val COUPON_COLLECTION = "coupon"
const val CODE_COLLECTION = "code"
const val OFF05 = "off_05"
const val OFF10 = "off_10"
const val OFF20 = "off_20"
const val OFF25 = "off_25"
const val OFF50 = "off_50"

// Paging
const val FEED_PAGINATION_SIZE = 10

// Treatment
const val TREATMENT_COLLECTION = "treatment"
const val PROGRESS_COLLECTION = "progress"
const val ACTIVE = "active"
const val STARTED = "started"
const val INIT_DATE = "initDate"
const val NO_TREATMENT = "no_current_treat_active"

// Contract
const val CONTRACT_COLLECTION = "contract"

/**
 * Body analyser parameter
 */

// Additional parameters
const val LOW = "Baixo"
const val NORMAL = "Normal"
const val HIGH = "Alto"
const val EXCELLENT = "Excelente"
const val VERY_HIGH = "Elevado"
const val LOW_WEIGHT = "Abaixo do peso"
const val HIGH_WEIGHT = "Acima do peso"
const val OBESE = "Obeso"

// Tables
const val IMC = "IMC"
const val IGV = "IGV"
const val BNA = "BNA"
const val BNE = "BNE"
const val BNAE = "BNAE"
