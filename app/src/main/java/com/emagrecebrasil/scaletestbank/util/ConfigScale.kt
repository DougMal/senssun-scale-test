package com.emagrecebrasil.scaletestbank.util

import android.app.Application
import android.util.Log
import com.google.gson.Gson
import senssun.blelib.model.BleDevice

object ConfigScale {

    lateinit var application: Application
    private var deviceAvailable: BleDevice? = null
    private var deviceAddress: String? = null

    @JvmStatic
    val bleDeviceAvailable: BleDevice?
        get() {
            val preferences = LocalConfig(application)
            val gson = Gson()
            val json = preferences.getConfig(SCALE_OBJECT)
            deviceAvailable = gson.fromJson(json, BleDevice::class.java)
            return deviceAvailable
        }

    @JvmStatic
    val bleDeviceAddress: String?
        get() {
            Log.d("TAG", "Application recuperada:")
            Log.d("TAG", "$application")
            val preferences = LocalConfig(application)
            deviceAddress = preferences.getConfig(SCALE_ADDRESS)
            return deviceAddress
        }
}