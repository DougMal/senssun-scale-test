package com.emagrecebrasil.scaletestbank.util


import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import com.emagrecebrasil.scaletestbank.R


sealed class DonutCategory(val name: String, @SuppressLint("SupportAnnotationUsage") @ColorRes val colorRes: Int)


@RequiresApi(Build.VERSION_CODES.O)
object ExtremeLowWeight: DonutCategory("Extremamente abaixo do peso",
        (R.color.extreme_low_weight))


@RequiresApi(Build.VERSION_CODES.O)
object SevereLowWeight: DonutCategory("Severamente abaixo do peso",
        (R.color.severe_low_weight))

@RequiresApi(Build.VERSION_CODES.O)
object LowWeight: DonutCategory("Abaixo do peso",
        (R.color.low_weight))

@RequiresApi(Build.VERSION_CODES.O)
object NormalWeight: DonutCategory("Peso normal",
        (R.color.normal_weight))

@RequiresApi(Build.VERSION_CODES.O)
object OverWeight: DonutCategory("Sobrepeso",
        (R.color.over_weight))

@RequiresApi(Build.VERSION_CODES.O)
object ObesityLowWeight: DonutCategory("Obesidade baixa",
        (R.color.obesity_low_weight))

@RequiresApi(Build.VERSION_CODES.O)
object ObesityHighWeight: DonutCategory("Obesidade severa",
        (R.color.obesity_high_weight))

@RequiresApi(Build.VERSION_CODES.O)
object MorbidWeight: DonutCategory("Obesidade mórbida",
        (R.color.morbid_obesity_weight))

    @RequiresApi(Build.VERSION_CODES.O)
    fun toHex(colorInt: Int) : String {
        val formatHex = "#%06X"
        return formatHex.format(colorInt)
    }