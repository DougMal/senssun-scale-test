package com.emagrecebrasil.scaletestbank.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class LocalConfig @SuppressLint("CommitPrefEdits") constructor(
        //File preferences
        private val cont: Context) {

    fun saveConfig(key: String?, value: String?) {
        editor.putString(key, value)
        editor.commit()
    }

    fun getConfig(key: String?): String? {
        return preferences.getString(key, "")
    }

    fun deleteAll() {
        editor.clear()
        editor.commit()
    }

    companion object {
        private lateinit var preferences: SharedPreferences
        private lateinit var editor: SharedPreferences.Editor
        const val FILE_NAME = "scale.test.bank.local.config"
        const val MODE_PRIVATE = 0
    }

    init {
        preferences = cont.getSharedPreferences(FILE_NAME, MODE_PRIVATE)
        editor = preferences.edit()
    }
}