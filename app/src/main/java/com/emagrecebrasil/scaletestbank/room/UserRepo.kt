package com.emagrecebrasil.scaletestbank.room

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.emagrecebrasil.scaletestbank.room.entity.User
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository
import com.emagrecebrasil.scaletestbank.ui.signup.FilledData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.text.SimpleDateFormat
import java.util.*

class UserRepo : KoinComponent {

    private val userRepository: UserRepository by inject()

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.O)
    fun saveNewUser(filledData: FilledData) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val userId = filledData.textFirebaseId
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val currentDate = sdf.format(Date())

            val localUser = User(
                userId = userId,
                name = filledData.textNome,
                surname = filledData.textSobrenome,
                email = filledData.textEmail,
                password = filledData.textSenha,
                dateInit = currentDate,
                birthday = filledData.textNascimento,
                height = filledData.textAltura.toInt(),
                desiredWeight = filledData.textPesoDesejado.toInt(),
                activityLevel = filledData.intActivity,
                gender = filledData.textGenero
            )

        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                Log.d(TAG, "Firebase return: ${e.message}")
            }
        }

    }

    fun getUserRoom(userId: String): User {
        return userRepository.getUser(userId)
    }

    companion object {
        private val TAG = UserRepo::class.java.simpleName
    }
}
