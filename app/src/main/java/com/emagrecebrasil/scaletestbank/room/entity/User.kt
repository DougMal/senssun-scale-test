package com.emagrecebrasil.scaletestbank.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User (
        @PrimaryKey
        val userId: String = "",
        val name: String = "",
        val surname: String = "",
        val email: String = "",
        val password: String = "",
        val dateInit: String = "",
        val birthday: String = "",
        val height: Int = 0,
        val desiredWeight: Int = 0,
        val activityLevel: Int = 0,
        val gender: String = ""
)
