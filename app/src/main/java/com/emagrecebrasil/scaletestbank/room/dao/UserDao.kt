package com.emagrecebrasil.scaletestbank.room.dao

import androidx.room.*
import com.emagrecebrasil.scaletestbank.room.entity.User

@Dao
interface UserDao {

    @Insert
    suspend fun insert(user: User) : Long

    @Update
    suspend fun update(user: User)

    @Delete
    suspend fun delete(user: User)

    @Query("DELETE FROM user_table")
    suspend fun deleteUserData()

    @Query("SELECT * FROM user_table")
    fun loadAllUsers() : List<User>

    @Query("SELECT * FROM user_table WHERE userId = :userId")
    fun loadUserData(userId: String): User

    @Query("SELECT EXISTS(SELECT * FROM user_table)")
    fun isTableExists() : Boolean
}