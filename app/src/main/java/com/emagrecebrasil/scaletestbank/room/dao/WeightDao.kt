package com.emagrecebrasil.scaletestbank.room.dao

import androidx.room.*
import com.emagrecebrasil.scaletestbank.room.entity.Weight
import java.util.*

@Dao
interface WeightDao {

    @Insert
    fun insert(weight: Weight) : Long

    @Update
    fun update(weight: Weight)

    @Delete
    fun delete(weight: Weight)

    @Query("DELETE FROM weight_table")
    fun deleteAll()

    @Query("SELECT * FROM weight_table")
    fun loadWeight(): List<Weight>

    @Query("SELECT EXISTS(SELECT * FROM weight_table)")
    fun isTableExists() : Boolean

    @Query("SELECT * FROM weight_table WHERE dbDate BETWEEN :dateIni AND :dateEnd")
    fun loadWeightPeriod(dateIni: Date, dateEnd: Date): List<Weight>

//    @Query("SELECT * FROM weight_table ORDER BY dbMeasureID DESC LIMIT 1 ")
    @Query("SELECT * FROM weight_table WHERE dbMeasureID=(SELECT MAX(dbMeasureID) FROM weight_table) ")
    fun loadLastWeight(): Weight

    @Query("SELECT * FROM weight_table WHERE dbMeasureID=(SELECT MIN(dbMeasureID) FROM weight_table) ")
    fun loadFirstWeight(): Weight

    @Query("SELECT * FROM weight_table WHERE dbWeight=(SELECT MIN(dbWeight) FROM weight_table) ")
    fun loadMinWeight(): Weight

    @Query("SELECT * FROM weight_table WHERE dbWeight=(SELECT MAX(dbWeight) FROM weight_table) ")
    fun loadMaxWeight(): Weight
}