package com.emagrecebrasil.scaletestbank.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "weight_table")
data class Weight (
        // Block related to the saving data
        val dbUserOwnerId: String,
        val dbDate: Date,
        val dbTime: String,

        // Block of data measured
        val dbWeight: Float,    // Measured Total Mass
        val dbImpedance: Float,  // Measured Impedance

        // Data calculated from Library
        val dbBMI: Float,          // Body Mass Index
        val dbSlim: Float,         // Slim body - %
        val dbSlimW: Float,        // Slim body - kg
        val dbVFat: Float,         // Visceral Fat
        val dbFat: Float,          // Body Fat - %
        val dbFatW: Float,         // Body Fat - kg
        val dbSubFat: Float,       // Subcutaneal Fat - %
        val dbSubFatW: Float,      // Subcutaneal Fat - kg
        val dbMoisture: Float,     // Water - %
        val dbMoistureW: Float,    // Water - kg
        val dbMuscle: Float,       // Muscle - %
        val dbMuscleW: Float,      // Muscle - kg
        val dbBone: Float,         // Bones - %
        val dbBoneW: Float,        // Bones - kg
        val dbProtein: Float,      // Protein - %
        val dbProteinW: Float,     // Protein - kg
        val dbSkeletalMuscle: Float,   // Skeletal Muscle - %
        val dbSkeletalMuscleW: Float,  // Skeletal Muscle - kg
        val dbBodyScore: Float,    // Body Score
        val dbBodyAge: Float,      // Body Age
        val dbBodyType: Float,     // Body Type
        val dbBMR: Float,          // Basal Metabolism Rate - kcal
        val dbAMR: Float           // Active Metabolism Rate - kcal
) {
    @PrimaryKey (autoGenerate = true)
    var dbMeasureID: Long = 0
}