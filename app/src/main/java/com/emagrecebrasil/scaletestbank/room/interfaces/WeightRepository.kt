package com.emagrecebrasil.scaletestbank.room.interfaces


import com.emagrecebrasil.scaletestbank.room.entity.Weight
import java.util.*

interface WeightRepository {

    fun insertWeight(weight: Weight) : Long

    fun updateWeight(weight: Weight)

    fun deleteWeight(weight: Weight)

    fun deleteAllWeight()

    fun getWeight(): List<Weight>

    fun getWeightPeriod(dateIni: Date, dateEnd: Date): List<Weight>

    fun getLastWeight(): Weight

    fun getFirstWeight(): Weight

    fun getMinWeight(): Weight

    fun getMaxWeight(): Weight

    fun isTableExists() : Boolean
}