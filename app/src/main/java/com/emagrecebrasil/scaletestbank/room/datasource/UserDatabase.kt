package com.emagrecebrasil.scaletestbank.room.datasource

import android.annotation.SuppressLint
import com.emagrecebrasil.scaletestbank.room.dao.UserDao
import com.emagrecebrasil.scaletestbank.room.entity.User
import com.emagrecebrasil.scaletestbank.room.interfaces.UserRepository

class UserDatabase(private val userDao: UserDao) : UserRepository {

    @SuppressLint("SimpleDateFormat")
    override suspend fun insertUser(user: User): Long {
        val localUser = User(
                userId = user.userId,
                name = user.name,
                surname = user.surname,
                email = user.email,
                password = user.password,
                dateInit = user.dateInit,
                birthday = user.birthday,
                height = user.height,
                desiredWeight = user.desiredWeight,
                activityLevel = user.activityLevel,
                gender = user.gender
        )
        return userDao.insert(localUser)
    }

    override suspend fun updateUser(user: User) {
        val localUser = User(
                userId = user.userId,
                name = user.name,
                surname = user.surname,
                email = user.email,
                password = user.password,
                dateInit = user.dateInit,
                birthday = user.birthday,
                height = user.height,
                desiredWeight = user.desiredWeight,
                activityLevel = user.activityLevel,
                gender = user.gender
        )

        userDao.update(localUser)
    }

    override suspend fun deleteUser(user: User) {
        userDao.delete(user)
    }

    override suspend fun deleteAllUser() {
        userDao.deleteUserData()
    }

    override fun getAllUsers(): List<User> {
        return userDao.loadAllUsers()
    }

    override fun getUser(userId: String): User {
        return userDao.loadUserData(userId)
    }

    override fun isTableExists(): Boolean {
        return userDao.isTableExists()
    }

}

