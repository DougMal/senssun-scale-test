package com.emagrecebrasil.scaletestbank.room.interfaces

import com.emagrecebrasil.scaletestbank.room.entity.User

interface UserRepository {

    suspend fun insertUser(user: User): Long

    suspend fun updateUser(user: User)

    suspend fun deleteUser(user: User)

    suspend fun deleteAllUser()

    fun getAllUsers() : List<User>

    fun getUser(userId: String): User

    fun isTableExists() : Boolean

}