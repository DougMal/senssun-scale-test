package com.emagrecebrasil.scaletestbank.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.emagrecebrasil.scaletestbank.room.dao.UserDao
import com.emagrecebrasil.scaletestbank.room.dao.WeightDao
import com.emagrecebrasil.scaletestbank.room.entity.User
import com.emagrecebrasil.scaletestbank.room.entity.Weight
import com.emagrecebrasil.scaletestbank.util.DATABASE_NAME
import java.util.concurrent.Executors
import com.emagrecebrasil.scaletestbank.room.Converters

@Database(entities = [User::class, Weight::class], version = 1 )
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract val userDao: UserDao
    abstract val weightDao: WeightDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDBInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {

                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
            }
            //buildDatabase(context)
            return INSTANCE!!
        }

    }

}