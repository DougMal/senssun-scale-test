package com.emagrecebrasil.scaletestbank.room.datasource

import com.emagrecebrasil.scaletestbank.room.dao.WeightDao
import com.emagrecebrasil.scaletestbank.room.entity.Weight
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import java.util.*

class WeightDatabase(private val weightDao: WeightDao) : WeightRepository {
    override fun insertWeight(weight: Weight): Long {
        val localWeight = Weight(
            dbUserOwnerId = weight.dbUserOwnerId,
            dbDate = weight.dbDate,
            dbTime = weight.dbTime,
            dbWeight = weight.dbWeight,
            dbImpedance = weight.dbImpedance,
            dbBMI = weight.dbBMI,
            dbVFat = weight.dbVFat,
            dbFat = weight.dbFat,
            dbFatW = weight.dbFatW,
            dbSlim = weight.dbSlim,
            dbSlimW = weight.dbSlimW,
            dbSubFat = weight.dbSubFat,
            dbSubFatW = weight.dbSubFatW,
            dbMoisture = weight.dbMoisture,
            dbMoistureW = weight.dbMoistureW,
            dbMuscle = weight.dbMuscle,
            dbMuscleW = weight.dbMuscleW,
            dbBone = weight.dbBone,
            dbBoneW = weight.dbBoneW,
            dbProtein = weight.dbProtein,
            dbProteinW = weight.dbProteinW,
            dbSkeletalMuscle = weight.dbSkeletalMuscle,
            dbSkeletalMuscleW = weight.dbSkeletalMuscleW,
            dbBodyScore = weight.dbBodyScore,
            dbBodyAge = weight.dbBodyAge,
            dbBodyType = weight.dbBodyType,
            dbBMR = weight.dbBMR,
            dbAMR = weight.dbAMR
        )
        return weightDao.insert(localWeight)
    }

    override fun updateWeight(weight: Weight) {
        val localWeight = Weight(
            dbUserOwnerId = weight.dbUserOwnerId,
            dbDate = weight.dbDate,
            dbTime = weight.dbTime,
            dbWeight = weight.dbWeight,
            dbImpedance = weight.dbImpedance,
            dbBMI = weight.dbBMI,
            dbVFat = weight.dbVFat,
            dbFat = weight.dbFat,
            dbFatW = weight.dbFatW,
            dbSlim = weight.dbSlim,
            dbSlimW = weight.dbSlimW,
            dbSubFat = weight.dbSubFat,
            dbSubFatW = weight.dbSubFatW,
            dbMoisture = weight.dbMoisture,
            dbMoistureW = weight.dbMoistureW,
            dbMuscle = weight.dbMuscle,
            dbMuscleW = weight.dbMuscleW,
            dbBone = weight.dbBone,
            dbBoneW = weight.dbBoneW,
            dbProtein = weight.dbProtein,
            dbProteinW = weight.dbProteinW,
            dbSkeletalMuscle = weight.dbSkeletalMuscle,
            dbSkeletalMuscleW = weight.dbSkeletalMuscleW,
            dbBodyScore = weight.dbBodyScore,
            dbBodyAge = weight.dbBodyAge,
            dbBodyType = weight.dbBodyType,
            dbBMR = weight.dbBMR,
            dbAMR = weight.dbAMR
        )
        weightDao.update(localWeight)
    }

    override fun deleteWeight(weight: Weight) {
        weightDao.delete(weight)
    }

    override fun deleteAllWeight() {
        weightDao.deleteAll()
    }

    override fun getWeight() : List<Weight> {
        return weightDao.loadWeight()
    }

    override fun getWeightPeriod(dateIni: Date, dateEnd: Date) : List<Weight> {
        return weightDao.loadWeightPeriod(dateIni, dateEnd)
    }

    override fun getLastWeight() : Weight {
        return weightDao.loadLastWeight()
    }

    override fun getFirstWeight(): Weight {
        return weightDao.loadFirstWeight()
    }

    override fun getMinWeight(): Weight {
        return weightDao.loadMinWeight()
    }

    override fun getMaxWeight(): Weight {
        return weightDao.loadMaxWeight()
    }

    override fun isTableExists(): Boolean {
        return weightDao.isTableExists()
    }
}