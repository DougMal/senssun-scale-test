package com.emagrecebrasil.scaletestbank.room

import android.annotation.SuppressLint
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.math.RoundingMode
import java.text.*
import java.util.*

/**
 * Type converters to allow Room to reference complex data types.
 */
class Converters {

    //Format numbers
    private val numberFormat: String = "###.#"
    private val decimalFormat: DecimalFormat = NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat

    val gson = Gson()

    @SuppressLint("SimpleDateFormat")
    var df: DateFormat = SimpleDateFormat("dd/MM/yyyy")

    @TypeConverter
    fun stringToDate(value: String?): Date? {
        return if (value != null) {
            try {
                return df.parse(value)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            null
        } else {
            null
        }
    }

    @TypeConverter
    fun dateToString(value: Date?): String? {
        return if (value == null) null else df.format(value)
    }

    @TypeConverter
    fun toDate(dateLong:Long): Date {
        return Date(dateLong)
    }

    @TypeConverter
    fun fromDate(date: Date): Long {
        return date.time;
    }

    //convert a map to a data class
    inline fun <reified T> Map<String, Any>.toDataClass(): T {
        return convert()
    }

    //convert a data class to a map
    fun <T> T.serializeToMap(): Map<String, Any> {
        return convert()
    }

    //convert an object of type I to type O
    inline fun <I, reified O> I.convert(): O {
        val json = gson.toJson(this)
        return gson.fromJson(json, object : TypeToken<O>() {}.type)
    }

    fun Float.fixDec() : String {
        decimalFormat.roundingMode = RoundingMode.CEILING
        decimalFormat.applyLocalizedPattern(numberFormat)
        return decimalFormat.format(this)
    }

}