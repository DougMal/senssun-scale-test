package com.emagrecebrasil.scaletestbank.room

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.emagrecebrasil.scaletestbank.R
import com.emagrecebrasil.scaletestbank.room.interfaces.WeightRepository
import com.emagrecebrasil.scaletestbank.ui.AdditionalData
import com.emagrecebrasil.scaletestbank.ui.MeasurementViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class GraphRepo(private val weightRepository: WeightRepository) : KoinComponent {

    //DI
    private val appContext: Context by inject()
    private val mViewModel: MeasurementViewModel by inject()

    //Format numbers
    private val numberFormat: String = "#.###,0"
    private val decimalFormat: DecimalFormat =
        NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat

    private var localLiveAdditionalData = MutableLiveData<AdditionalData>()
    fun getAdditionals(): MutableLiveData<AdditionalData> {
        val tmpAdd = AdditionalData()
        val tmpWeight = weightRepository.getLastWeight()
        val userGender = mViewModel.getUserGender()
        val slimFat = 1 - tmpWeight.dbFat / tmpWeight.dbWeight

        // Prepare data
        tmpAdd.apply {
            addBMI = tmpWeight.dbBMI.fixDec()
            addBodyFat = tmpWeight.dbFat.fixDec()
            addBodyFatW = tmpWeight.dbFatW.fixDec()
            addWater = tmpWeight.dbMoisture.fixDec()
            addWaterW = tmpWeight.dbMoistureW.fixDec()
            addMuscleSkel = tmpWeight.dbSkeletalMuscle.fixDec()
            addMuscleSkelW = tmpWeight.dbSkeletalMuscleW.fixDec()
            addBones = tmpWeight.dbBone.fixDec()
            addBonesW = tmpWeight.dbBoneW.fixDec()
            addProtein = tmpWeight.dbProtein.fixDec()
            addProteinW = tmpWeight.dbProteinW.fixDec()
            addMuscle = tmpWeight.dbMuscle.fixDec()
            addMuscleW = tmpWeight.dbMuscleW.fixDec()
            addVfat = tmpWeight.dbVFat.fixDec()
            addSubFat = tmpWeight.dbSubFatW.fixDec()
            addSlimFatW = (tmpWeight.dbWeight - tmpWeight.dbFatW).fixDec()
            addBMR = tmpWeight.dbBMR.fixDec()
            addAMR = tmpWeight.dbAMR.fixDec()

            /**
             * Gauge type
             * BNA = 1
             * BNE = 2
             * IGV = 3
             * BNAE = 4
             * IMC = 5
             */
            addBMIResult = calculateResult(tmpWeight.dbBMI, rangeBMI, 5)
            if (userGender == "Male") {
                addBodyFatResult = calculateResult(tmpWeight.dbFat, rangeBodyFatM, 4)
                addWaterResult = calculateResult(tmpWeight.dbMoisture, rangeWaterM, 1)
                addMuscleSkelResult = calculateResult(tmpWeight.dbSkeletalMuscle, rangeMuscleSkelM, 2)
                addMuscleResult = calculateResult(tmpWeight.dbMuscle, rangeMuscleM, 2)
            } else {
                addBodyFatResult = calculateResult(tmpWeight.dbFat, rangeBodyFatF, 4)
                addWaterResult = calculateResult(tmpWeight.dbMoisture, rangeWaterF, 1)
                addMuscleSkelResult = calculateResult(tmpWeight.dbSkeletalMuscle, rangeMuscleSkelF, 2)
                addMuscleResult = calculateResult(tmpWeight.dbMuscle, rangeMuscleF, 2)
            }
            addBonesResult = calculateResult(tmpWeight.dbBone, rangeBones, 1)
            addProteinResult = calculateResult(tmpWeight.dbProtein, rangeProtein, 1)
            addVfatResult = calculateResult(tmpWeight.dbVFat, rangeVFat, 3)
            addSubFatResult = calculateResult(tmpWeight.dbSubFat, rangeSubFat, 1)
            addSlimFatResult = calculateResult(slimFat, rangeSlimFat, 4)
        }

        localLiveAdditionalData.value = tmpAdd
        return localLiveAdditionalData
    }

    private fun calculateResult(measure: Float, range: Array<Float>, gauge: Int): String {
        var position: Int = 1
        var output = ""
        range.forEach { limit ->
            if (measure >= limit) {
                position += 1
            }
        }
        when (gauge) {
            1 -> {
                when (position) {
                    1 -> output = appContext.getString(R.string.add_low)
                    2 -> output = appContext.getString(R.string.add_normal)
                    3 -> output = appContext.getString(R.string.add_high)
                }
            }
            2 -> {
                when (position) {
                    1 -> output = appContext.getString(R.string.add_low)
                    2 -> output = appContext.getString(R.string.add_normal)
                    3 -> output = appContext.getString(R.string.add_excel)
                }
            }
            3 -> {
                when (position) {
                    1 -> output = appContext.getString(R.string.add_normal)
                    2 -> output = appContext.getString(R.string.add_high)
                    3 -> output = appContext.getString(R.string.add_very)
                }
            }
            4 -> {
                when (position) {
                    1 -> output = appContext.getString(R.string.add_low)
                    2 -> output = appContext.getString(R.string.add_normal)
                    3 -> output = appContext.getString(R.string.add_high)
                    4 -> output = appContext.getString(R.string.add_very)
                }
            }
            5 -> {
                when (position) {
                    1 -> output = appContext.getString(R.string.add_bodyshape_1)
                    2 -> output = appContext.getString(R.string.add_bodyshape_2)
                    3 -> output = appContext.getString(R.string.add_bodyshape_3)
                    4 -> output = appContext.getString(R.string.add_bodyshape_4)
                    5 -> output = appContext.getString(R.string.add_bodyshape_5)
                    6 -> output = appContext.getString(R.string.add_bodyshape_6)
                    7 -> output = appContext.getString(R.string.add_bodyshape_7)
                    8 -> output = appContext.getString(R.string.add_bodyshape_8)
                }
            }
        }
        return output
    }

    private fun Float.fixDec(): String {
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        decimalFormat.applyLocalizedPattern(numberFormat)
        return decimalFormat.format(this)
    }

    companion object {
        private val rangeBMI = arrayOf(13.9f, 16.9f, 18.4f, 24.9f, 29.9f, 34.9f, 39.9f, 100.0f)
        private val rangeBodyFatM = arrayOf(5f, 15f, 25f, 100f)
        private val rangeBodyFatF = arrayOf(8f, 23f, 32f, 100f)
        private val rangeWaterM = arrayOf(50f, 62.5f, 69f)
        private val rangeWaterF = arrayOf(45f, 56.5f, 62f)
        private val rangeMuscleSkelM = arrayOf(7f, 11f, 30f)
        private val rangeMuscleSkelF = arrayOf(5.7f, 8f, 30f)
        private val rangeBones = arrayOf(1.8f, 2.21f, 10f)
        private val rangeProtein = arrayOf(12f, 20f, 30f)
        private val rangeMuscleM = arrayOf(70f, 89f, 100f)
        private val rangeMuscleF = arrayOf(60f, 75.5f, 100f)
        private val rangeVFat = arrayOf(12f, 30f, 59f)
        private val rangeSubFat = arrayOf(10f, 20f, 30f)
        private val rangeSlimFat = arrayOf(10f, 20f, 30f, 40f, 100f)
        private val TAG = GraphRepo::class.java.simpleName
    }
}